import sbt.Keys.libraryDependencies
import versions._

ThisBuild / scalaVersion := "2.13.1"
ThisBuild / organization := "org.halla"
ThisBuild / scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xfatal-warnings")
ThisBuild / javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

lazy val compiler = project
  .dependsOn(lang)
  .settings(

    // Resolver for Parseback
    resolvers += "bintray-djspiewak-maven" at "https://dl.bintray.com/djspiewak/maven",

    // Parseback: Used to generate an AST from Halla syntax (see `ast` package for usage)
    libraryDependencies += "com.codecommit" %% "parseback" % parsebackVersion,

    // ASM: Used to assemble java binaries
    libraryDependencies += "org.ow2.asm" % "asm" % ow2asmVersion,

    // Monix: Used for safer concurrency and to speed up certain tasks via simple parallel processing
    libraryDependencies += "io.monix" %% "monix" % monixVersion,

    // Monocle: Used to make editing nested case classes much easier
    libraryDependencies ++= Seq(
      "com.github.julien-truffaut" %%  "monocle-core"  % monocleVersion,
      "com.github.julien-truffaut" %%  "monocle-macro" % monocleVersion,
      "com.github.julien-truffaut" %%  "monocle-law"   % monocleVersion % Test
    ),

    // Logging:
    libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
    libraryDependencies += "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.11.1" % Runtime, // bridge: slf4j -> log4j
    libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.11.1" % Runtime,        // log4j as logging mechanism
    libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.11.1" % Runtime,       // log4j as logging mechanism

    // Testing:
    libraryDependencies += "org.scalatest" %% "scalatest" % scalatestVersion % Test,

    // ASM Utils: Utiliies used to debug generated code
    libraryDependencies += "org.ow2.asm" % "asm-util" % ow2asmVersion % Test

  )

lazy val lang = project
  .settings(
      // Testing:
      libraryDependencies += "org.scalatest" %% "scalatest" % scalatestVersion % Test,
  )