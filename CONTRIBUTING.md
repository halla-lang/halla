# Merge requests workflow

We welcome merge requests from everyone, with fixes and improvements
to Halla code, tests, and documentation.

## Merge request guidelines

If you find an issue, please submit a merge request with a fix or improvement, if
you can, and include tests. If you don't know how to fix the issue but can write a test
that exposes the issue, we will accept that as well. In general, bug fixes that
include a regression test are merged quickly, while new features without proper
tests might be slower to receive feedback. The workflow to make a merge
request is as follows:

1. [Fork](../../user/project/repository/forking_workflow.md) the project into
   your personal namespace (or group) on GitLab.com.
1. Create a feature branch in your fork (don't work off `master`).
1. Write [tests](../rake_tasks.md#run-tests) and code.
1. [Generate a changelog entry with `bin/changelog`](../changelog.md)
1. If you are writing documentation, make sure to follow the
   [documentation guidelines](../documentation/index.md).
1. Follow the [commit messages guidelines](#commit-messages-guidelines).
1. If you have multiple commits, combine them into a few logically organized
   commits by [squashing them](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History#_squashing),
   but do not change the commit history if you're working on shared branches though.
1. Push the commit(s) to your working branch in your fork.
1. Submit a merge request (MR) to the `master` branch in the main GitLab project.
   1. Your merge request needs at least 1 approval, but feel free to require more.
   1. You don't have to select any specific approvers, but you can if you really want
      specific people to approve your merge request.
1. The MR title should describe the change you want to make.
1. The MR description should give a reason for your change.
   1. If you are contributing code, fill in the description according to the default
      template already provided in the "Description" field.
   1. If you are contributing documentation, choose `Documentation` from the
      "Choose a template" menu and fill in the description according to the template.
   1. Mention the issue(s) your merge request solves, using the `Solves #XXX` or
      `Closes #XXX` syntax to [auto-close](../../user/project/issues/managing_issues.md#closing-issues-automatically)
      the issue(s) once the merge request is merged.
1. If you're allowed to (Core team members, for example), set a relevant milestone
   and [labels](issue_workflow.md).
1. Be prepared to answer questions and incorporate feedback into your MR with new
   commits. Once you have fully addressed a suggestion from a reviewer, click the
   "Resolve thread" button beneath it to mark it resolved.
   1. The merge request author resolves only the threads they have fully addressed.
      If there's an open reply or thread, a suggestion, a question, or anything else,
      the thread should be left to be resolved by the reviewer.

### Keep it simple

*Live by smaller iterations.*

Small MRs which are more easily reviewed, lead to higher code quality which is
more important to Halla than having a minimal commit log. The smaller an MR is,
the more likely it will be merged quickly. After that you can send more MRs to
enhance and expand the feature. The [How to get faster PR reviews](https://github.com/kubernetes/kubernetes/blob/release-1.5/docs/devel/faster_reviews.md)
document from the Kubernetes team also has some great points regarding this.

### Commit messages guidelines

When writing commit messages, please follow the guidelines below:

- The commit subject must contain at least 3 words.
- The commit subject should ideally contain up to 50 characters,
  and must not be longer than 72 characters.
- The commit subject must start with a capital letter.
- The commit subject must not end with a period.
- The commit subject and body must be separated by a blank line.
- The commit body must not contain more than 72 characters per line.
- Commits that change 30 or more lines across at least 3 files must
  describe these changes in the commit body.
