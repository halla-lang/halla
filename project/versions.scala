/**
 * Versions for dependencies
 */
object versions {
  val monocleVersion = "2.0.4"
  val parsebackVersion = "0.4.0"
  val monixVersion = "3.1.0"
  val ow2asmVersion = "4.0"
  val scalatestVersion = "3.1.2"
}
