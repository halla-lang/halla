package org.halla.compiler.assemble.compose

import org.halla.compiler.assemble.asm.specs.ClassSpecifications.{Clazz, Interface}
import org.halla.compiler.assemble.asm.visit.Method
import org.halla.compiler.assemble.asm.{ASMResult, ContextualBuilder}
import org.halla.compiler.assemble.context.Composition.{CaseMatchToAssemble, CompositionContext}
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.lambdify.lambda.Type.TypeUnion


object TypeComposer {

  /**
    * Compose java interfaces and classes for all the classes that were detected in the code to be compiled, as well as
    * the full list of match statement cases that were found by the [[MethodComposer]]
    *
    * @param context assembly context with all the types that need to be assembled
    * @param casesToAssemble match statement cases that will be addressed via abstract methods on [[Type.TypeUnion]]s
    * @return
    */
  def compose(context: CompositionContext)(casesToAssemble: Seq[CaseMatchToAssemble]): (Seq[ASMResult], Seq[ASMResult]) = {

    // get all the unique abstract methods for each case, and map them to the case type interfaces that must own them
    val abstractMethodsByCaseInterface: Map[Type.TypeUnion.Declared, Seq[Method]] = casesToAssemble
      .map(c => c.t -> c.abstractMethod)
      .groupBy(_._1)
      .view
      .mapValues(_.map(_._2))
      .toMap

    // build a java interface for each case type interface
    val interfaces: Map[TypeUnion.Declared, Interface] = abstractMethodsByCaseInterface.map({ case (interface, abstractMethods) =>

      // TODO figure out how to namespace types
      val nameSpace = Seq("udt")

      // create an interface for the type
      interface -> Interface(nameSpace = nameSpace, className = interface.symbol, methods = abstractMethods)
    })

    // assemble classes for each of the interfaces
    val runtimeTypes = interfaces.values.map(ContextualBuilder.defineClazz(_)(context.builderContext))

    // build map from each case type to it's respective interface
    val unionInterfaces = interfaces.flatMap({ case (interface, asmInterface) =>
      interface.types.map(caseType => caseType -> asmInterface).toList
    })

    // get all of the cases by case type
    val caseTypes = casesToAssemble
      .flatMap(_.branches)
      .groupBy(_._1)
      .view
      .mapValues(_.map(_._2)).map({ case (caseType, methods) => {
      // TODO figure out how to namespace types
      val nameSpace = Seq("udt")

      val interface = unionInterfaces.getOrElse(caseType, { throw new RuntimeException("CaseType's interface could not be found.") })

      // create an class for the case type
      val caseTypeClazz = Clazz(nameSpace = nameSpace, className = caseType.symbol, implements = Seq(interface), defaultConstructor = true, methods = methods)

      ContextualBuilder.defineClazz(caseTypeClazz)(context.builderContext)
    }}).toSeq

    // return both the runtime types and case types
    (runtimeTypes.toSeq, caseTypes)
  }

}
