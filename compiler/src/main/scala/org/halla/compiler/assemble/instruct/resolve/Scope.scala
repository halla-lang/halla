package org.halla.compiler.assemble.instruct.resolve

import org.halla.compiler.assemble.blueprint.ValueBlueprint.Values
import org.halla.compiler.assemble.blueprint.{FunctionBlueprint, ValueBlueprint}

case class Scope(levels: Seq[Values] = Seq()) {

  /**
    * Create a copy of this scope with the new level. Each level must contain at least 1 [[ValueBlueprint]].
    *
    * @param level to add
    * @return a copy of this scope if the level is not empty, otherwise returns the original scope.
    */
  def add(level: Values): Scope = {
    // if the level we want to add is empty
    if (level.isEmpty) {
      // return without adding a new level of scope
      return this
    }

    // otherwise, return a copy of this scope with the new level added
    Scope(levels.:+(level))
  }
}


object Scope {

  /**
    * Resolved reference
    */
  type ResolvedRef = Either[ValueBlueprint, FunctionBlueprint]

  import scala.language.implicitConversions

  implicit def toScope(levels: Iterable[Values]): Scope = Scope(levels.toSeq)
  implicit def toTopLevelScope(level: Values): Scope = Scope().add(level)
}
