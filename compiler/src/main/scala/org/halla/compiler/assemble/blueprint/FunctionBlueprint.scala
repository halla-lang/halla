package org.halla.compiler.assemble.blueprint

import org.halla.compiler.assemble.blueprint.ValueBlueprint.Values
import org.halla.compiler.assemble.instruct.StackModifier
import org.halla.compiler.assemble.instruct.resolve.InstructionGraph
import org.halla.compiler.parse.ast.expr.Expression.Ref
import org.halla.compiler.parse.parsed.Parsed
import org.halla.compiler.parse.parsed.Parsed.SrcMapped

final case class FunctionBlueprint(name: String, graph: InstructionGraph, parameters: Values)
                                  (override val src: Seq[Parsed]) extends SrcMapped {
  def ref: Ref = Ref(name)(src.flatMap(_.loc).toList)
  val accepts: Seq[StackModifier] = parameters.map(_.modifier)
  def returns: StackModifier = graph.result
}