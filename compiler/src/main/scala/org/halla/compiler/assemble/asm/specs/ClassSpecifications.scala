package org.halla.compiler.assemble.asm.specs

import org.halla.compiler.assemble.asm.BuilderContext
import org.halla.compiler.assemble.asm.visit.Method

sealed trait ClassSpecifications {
  val nameSpace: Seq[String]
  val className: String

  /**
    * Compute the literal class path the class will have in written ASM
    * @param builderContext asm builder context
    * @return
    */
  def computePath(implicit builderContext: BuilderContext): String = s"${builderContext.compilerPrefix}/${nameSpace.mkString("/")}/$className"
}

object ClassSpecifications {

  /**
    * Specifications for a class.
    * @param nameSpace namespace to place the class in
    * @param className name of the class
    * @param implements interfaces the class implements
    * @param defaultConstructor if true, a default constructor will be created for the class
    * @param methods methods to be added to the class
    */
  final case class Clazz( nameSpace: Seq[String],
                          className: String,
                          implements: Iterable[Interface] = Seq(),
                          defaultConstructor: Boolean = false,
                          methods: Iterable[Method] = Seq()
                        ) extends ClassSpecifications

  /**
    * Specifications for an interface.
    * @param nameSpace namespace to place the interface in
    * @param className name of the interface
    * @param methods methods to be added to the class
    */
  final case class Interface( nameSpace: Seq[String],
                              className: String,
                              methods: Iterable[Method] = Seq()
                            ) extends ClassSpecifications

}
