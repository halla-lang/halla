package org.halla.compiler.assemble.asm.visit

import org.halla.compiler.assemble.asm.visit.invoke.Invocation
import org.objectweb.asm.Type

sealed trait StackConsumer

object StackConsumer {

  /**
    * Invoke a function (like adding integers for instance)
    * @param target to invoke
    */
  case class Invoke(target: Invocation) extends StackConsumer

  /**
    * Keeps values on the stack for use later. This is used for returning values without modifying them.
    * For instance, returning literals.
    */
  case object Keep extends StackConsumer

  /**
    * Visits return opcode for the provided type
    * @param returnType to provide a return opcode for
    */
  case class Return(returnType: Option[Type]) extends StackConsumer

}
