package org.halla.compiler.assemble

import com.typesafe.scalalogging.LazyLogging
import org.halla.compiler.assemble.asm.ASMResult.Compiled
import org.halla.compiler.assemble.asm.specs.ClassSpecifications.Clazz
import org.halla.compiler.assemble.asm.{ASMResult, ContextualBuilder}
import org.halla.compiler.assemble.blueprint.PackageBlueprint
import org.halla.compiler.assemble.compose.{MethodComposer, TypeComposer}
import org.halla.compiler.assemble.context.Composition

/**
  * The [[Assembler]] turns a Blueprint produced by the Resolver into bytecode, this bytecode can then be used by the
  * Halla Runtime (defined in [[org.halla.lang]]) as well any Java/Scala program.
  */
object Assembler extends LazyLogging {

  import org.halla.compiler.assemble.context.Composition._

  case class AssemblerResult(runtimePackages: Iterable[ASMResult], runtimeTypes: Iterable[ASMResult], runtimeCases: Iterable[ASMResult])

  /**
    * Attempt to compile blueprints into executable bytecode.
    *
    * This function will also write the [[Compiled]] bytecode out to a file but that might change in the near future.
    *
    * @param blueprints to compile
    * @return compilation results
    */
  def compile(blueprints: PackageBlueprint*): AssemblerResult = {

    // compile all each of the blueprints functions before considering user defined types
    Composition.collect(blueprints.map(pkg => {

      // iterate through each of the functions that need assembly.
      Composition.collect(pkg.functions.values.map(function => {
        // and compose each of them.
        MethodComposer.compose(function)
      })).map(methods => {
        // place all the methods in a class that will be assembled later in this method
        Clazz(nameSpace = pkg.namespace.path.reverse.tail, className = pkg.namespace.path.reverse.head, methods = methods)
      })

      // now that all the blueprints have been composed into java classes,
      // we can start writing the ASM for them.
    })).eval((classes, context) => {

      // write all the classes created by the MethodComposer into ASM
      val runtimePackages = classes.map(classSpecs => {
        ContextualBuilder.defineClazz(classSpecs)(context.builderContext)
      }).toSeq

      // lazily compose and assemble all the classes that were required by the composed methods
      val (runtimeTypes, runtimeCaseTypes) = TypeComposer.compose(context)(context.cases)

      // place all this assembly into a single container and return it
      AssemblerResult(runtimePackages, runtimeTypes, runtimeCaseTypes)
    })
  }
}