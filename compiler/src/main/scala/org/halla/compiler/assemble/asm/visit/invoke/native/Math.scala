package org.halla.compiler.assemble.asm.visit.invoke.native

import org.halla.compiler.assemble.asm.visit.ProviderChain.ProviderChain
import org.halla.compiler.assemble.asm.visit.invoke.Invocation
import org.halla.compiler.assemble.asm.visit.provide.StackProvider.Literal.IntL
import org.objectweb.asm.Opcodes.{BIPUSH, IADD, INVOKESTATIC, SIPUSH}
import org.objectweb.asm.{MethodVisitor, Type}

object Math {

  case object Addition extends Invocation {
    override def visit(mv: MethodVisitor, providers: ProviderChain): Unit = {
      providers match {
        case Seq(IntL(a), IntL(b)) =>
          // visit literal of type `int`
          mv.visitIntInsn({ if (a > 127) { SIPUSH } else { BIPUSH }}, a)

          // visit literal of type `int`
          mv.visitIntInsn({ if (b > 127) { SIPUSH } else { BIPUSH }}, b)

          // visit return statement for the method's expected return type
          mv.visitInsn(IADD)

          // convert to java.lang.Integer
          mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", s"(I)${Type.getDescriptor(classOf[Integer])}")
        case _ =>
          ???
      }
    }

    /**
     * The type returned by the function this [[Invocation]] targets.
     *
     * @return the return type
     */
    override def returns: Option[Type] = Some(Type.getType(classOf[Integer]))
  }
}
