package org.halla.compiler.assemble.asm.visit.provide

import org.halla.compiler.assemble.asm.visit.ProviderChain
import org.halla.compiler.assemble.asm.visit.provide.StackProvider.Literal._
import org.objectweb.asm.{MethodVisitor, Type}

object Literal {
  trait Builder[R] extends StackProvider.Builder[R] {
    def integer(i: Int): ProviderChain.Builder[R] = {
      this.add(IntL(i))
    }

    def string(s: String): ProviderChain.Builder[R] = {
      this.add(StringL(s))
    }

    def float(f: Float): ProviderChain.Builder[R] = {
      this.add(FloatL(f))
    }
  }

  def visit(mv: MethodVisitor, literal: Literal): Unit = {
    import org.objectweb.asm.Opcodes._

    literal match {

      case IntL(value) =>
        // visit literal of type `int`
        if (value <= 127 && value >= -128) { mv.visitIntInsn(BIPUSH, value) } else
        if (value <= 32767 && value >= -32768) { mv.visitIntInsn(SIPUSH, value)} else
        { mv.visitLdcInsn(value) }

        // convert to java.lang.Integer
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Integer", "valueOf", s"(I)${Type.getDescriptor(classOf[Integer])}")

      case StringL(value) =>
        // visit literal of type `String`
        mv.visitLdcInsn(value)

      case FloatL(value) =>
        // visit literal of type `float`
        mv.visitLdcInsn(value)
        // convert to java.lang.Float
        mv.visitMethodInsn(INVOKESTATIC, "java/lang/Float", "valueOf", s"(F)${Type.getDescriptor(classOf[java.lang.Float])}")
    }
  }
}
