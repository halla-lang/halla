package org.halla.compiler.assemble.asm.visit.invoke

import org.halla.compiler.assemble.asm.visit.ProviderChain.ProviderChain
import org.objectweb.asm.{MethodVisitor, Type}

trait Invocation {

  /**
   * When called, this function should generate bytecode that represents the invocation of a function.
   * @param mv [[MethodVisitor]] that will be used to write the bytecode
   * @param providers a chain of values that have been added to the stack previously.
   */
  def visit(mv: MethodVisitor, providers: ProviderChain): Unit

  /**
   * The type returned by the function this [[Invocation]] targets.
   * @return the return type
   */
  def returns: Option[Type]
}
