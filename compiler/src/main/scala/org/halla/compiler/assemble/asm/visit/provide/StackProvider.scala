package org.halla.compiler.assemble.asm.visit.provide

import org.halla.compiler.assemble.asm.visit.ProviderChain
import org.objectweb.asm.{MethodVisitor, Type}

trait StackProvider {
  val returnType: Type
  def visit(mv: MethodVisitor): Unit
}

object StackProvider {

  trait Builder[R] {
    protected def add(stackProvider: StackProvider): ProviderChain.Builder[R]
    def end(): R
  }

  object Literal {
    sealed trait Literal extends StackProvider {
      def visit(mv: MethodVisitor): Unit = {
        org.halla.compiler.assemble.asm.visit.provide.Literal.visit(mv, this)
      }
    }
    case class IntL(value: Integer) extends Literal {
      val returnType: Type = Type.getType(classOf[java.lang.Integer])
    }
    case class StringL(value: String) extends Literal {
      val returnType: Type = Type.getType(classOf[java.lang.String])
    }
    case class FloatL(value: java.lang.Float) extends Literal {
      val returnType: Type = Type.getType(classOf[java.lang.Float])
    }
  }
}
