package org.halla.compiler.assemble.asm.visit.invoke.interface

import org.halla.compiler.assemble.asm.visit.ProviderChain.ProviderChain
import org.halla.compiler.assemble.asm.visit.invoke.Invocation
import org.objectweb.asm.{MethodVisitor, Type}

case class InterfaceInvocation(owner: String, name: String, returns: Option[Type]) extends Invocation {
  import jdk.internal.org.objectweb.asm.Opcodes._

  /**
    * Visit the invocation of an interface
    * @param mv visitor
    * @param providers visitations that represent the stack up to this point
    */
  override def visit(mv: MethodVisitor, providers: ProviderChain): Unit = {
//    val parameterList = providers.map(p => p.returnType.getDescriptor).mkString
    val parameterList = ""

    // descriptor for the method we are invoking
    val descriptor = s"""($parameterList)${returns.map(_.getDescriptor).getOrElse("V")}"""

    // visit the arguments to this method, this will allow the arguments to be pushed onto the stack
    providers.foreach(_.visit(mv))

    // visit the invocation itself, so the arguments can be applied to it
    mv.visitMethodInsn(INVOKEINTERFACE, owner, name, descriptor)
  }
}
