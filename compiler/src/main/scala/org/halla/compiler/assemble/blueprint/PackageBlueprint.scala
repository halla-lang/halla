package org.halla.compiler.assemble.blueprint

import cats.Semigroup
import org.halla.compiler.lambdify.lambda.Type.ConcreteType
import org.halla.compiler.parse.ast.expr.Expression.Ref
import org.halla.compiler.parse.parsed.NS.NameSpace

case class PackageBlueprint(namespace: NameSpace, requires: Set[PackageBlueprint], types: List[ConcreteType], functions: Map[Ref, FunctionBlueprint])

object PackageBlueprint {
  import org.halla.compiler.parse.ast.Package

  /**
    * create base blueprint from package.
    *
    * It will start with just the namespace and name filled out, but as resolution continues, more and more of the
    * contents of the resolution context should be transferred to it.
    *
    * @param `package` to copy basic fields from
    * @return
    */
  def fromPackage(`package`: Package): PackageBlueprint =
    PackageBlueprint(
      namespace = `package`.namespace,
      requires = Set(),
      types = List.empty,
      functions = Map()
    )

  // implicits used to merge blueprint
  implicit private val functionBlueprintSemigroup: Semigroup[FunctionBlueprint] = (x: FunctionBlueprint, y: FunctionBlueprint) => x
  implicit private val refSemigroup: Semigroup[Ref] = (x: Ref, y: Ref) => x

  lazy implicit protected[compiler] val packageBlueprintMonoid: Semigroup[PackageBlueprint] = (x: PackageBlueprint, y: PackageBlueprint) => {
    import cats.implicits._
    x.copy(
      types = x.types ++ y.types,
      functions = x.functions.combine(y.functions)
    )
  }
}
