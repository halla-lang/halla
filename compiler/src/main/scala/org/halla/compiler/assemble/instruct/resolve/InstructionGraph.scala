package org.halla.compiler.assemble.instruct.resolve

import org.halla.compiler.assemble.blueprint.ValueBlueprint
import org.halla.compiler.assemble.instruct.StackModifier
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.parse.parsed.Parsed
import org.halla.compiler.parse.parsed.Parsed.SrcMapped

sealed trait InstructionGraph extends SrcMapped {
  def result: StackModifier
}

object InstructionGraph {

  /**
   * References a defined value such as a variable or function parameter and adds it to the stack.
   *
   * @param ref the reference that should exist earlier in the application chain (see the `parent` field)
   * @param src reference to source code for debugging and error handling
   */
  case class Reference(ref: ValueBlueprint)(val src: Seq[Parsed]) extends InstructionGraph {
    override val result: StackModifier = ref.modifier
  }

  /**
    * An [[Application]] graph represents the invocation of a function, with or without arguments.
    *
    * @param symbol function to invoke
    * @param parameters to invoke function with
    */
  case class Application(symbol: String, parameters: Seq[InstructionGraph], result: StackModifier)(override val src: Seq[Parsed]) extends InstructionGraph

  /**
    * A [[Directed]] graph contains a set of instructions that form a directed acyclic graph.
    *
    * Currently these are only created by match statements.
    *
    */
  sealed trait Directed extends InstructionGraph

  object Directed {

    /**
      * Represents matching of a subject as an instance of its supertype to each of its case types.
      *
      * @param result end of the match statement or in other words its returned value
      * @param subject to be pattern matched
      * @param branches match cases and their results
      */
    case class AlgebraicTypeMatch(result: StackModifier, subject: InstructionGraph, subjectType: Type.TypeUnion.Declared, branches: Map[Type.Composite, InstructionGraph])(override val src: Seq[Parsed]) extends Directed

  }

}


