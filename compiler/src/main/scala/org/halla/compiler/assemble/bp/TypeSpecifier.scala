package org.halla.compiler.assemble.bp

import org.halla.compiler.assemble.blueprint.ValueBlueprint
import org.halla.compiler.assemble.context.Composition
import org.halla.compiler.assemble.context.Composition.Basic
import org.halla.compiler.assemble.instruct.StackModifier
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.lambdify.lambda.Type.{ConcreteType, TypeUnion}
import org.objectweb.asm

object TypeSpecifier {

  def getOrDefineType(valueBlueprint: ValueBlueprint): Composition[asm.Type] = {
    getOrDefineType(valueBlueprint.modifier)
  }

  def getOrDefineType(stackModifier: StackModifier): Composition[asm.Type] = {
    (stackModifier.consumes, stackModifier.produces) match {
      case (Seq(), Seq(singleType)) =>
        getOrDefineType(singleType)

      case _ =>
        ???
    }
  }

  def getOrDefineType(typeBlueprint: ConcreteType): Composition[asm.Type] = {
    typeBlueprint match {
      case Type.Composite(typeRef, args) =>
        // create basic composition that will add this type to the context
        Basic(asm.Type.getType(s"Lhalla/udt/${typeBlueprint.symbol};"), identity)
      case native: Type.Native =>
        ???
//        native.t

      case TypeUnion.Declared(typeDef, types) =>
        // create basic composition that will add this type to the context
        Basic(asm.Type.getType(s"Lhalla/udt/${typeBlueprint.symbol};"), identity)
    }
  }

}
