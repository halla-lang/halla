package org.halla.compiler.assemble.blueprint

import org.halla.compiler.assemble.instruct.StackModifier
import org.halla.compiler.assemble.instruct.StackModifier.producing
import org.halla.compiler.lambdify.lambda.Type.ConcreteType
import org.halla.compiler.parse.ast.expr.Expression.Ref

/**
  * A value blueprint represents variables and how they are defined
  */
sealed trait ValueBlueprint {
  val modifier: StackModifier
}

object ValueBlueprint {
  
  type Values = List[ValueBlueprint]

  /**
    * A value defined as function parameter
    * @param index of the parameter
    * @param modifier type of the parameter
    */
  case class FunctionArgument(index: Int, modifier: StackModifier) extends ValueBlueprint

  /**
    * A declared literal of some native type
    */
  case class Literal(value: Any, typeBlueprint: ConcreteType) extends ValueBlueprint {
    lazy val modifier: StackModifier = producing(typeBlueprint)
  }

  /**
    * Check references for a matching ref
    * @param ref to look for
    * @param references to check
    * @return maybe the reference blueprint
    */
  def checkForRef(ref: Ref, references: Values): Option[ValueBlueprint] = {
    ???
  }
}
