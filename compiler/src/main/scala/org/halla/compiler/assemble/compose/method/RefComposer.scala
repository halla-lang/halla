package org.halla.compiler.assemble.compose.method

import org.halla.compiler.assemble.asm.visit.ProviderChain
import org.halla.compiler.assemble.blueprint.ValueBlueprint
import org.halla.compiler.assemble.blueprint.ValueBlueprint._

object RefComposer {
  def compose[R](value: ValueBlueprint)(implicit providedWriter: ProviderChain.Builder[R]): ProviderChain.Builder[R] = {
    value match {

      case FunctionArgument(index, modifier) =>
        providedWriter.argument(index)

      case Literal(value, typeBlueprint) =>
        LiteralComposer.compose(providedWriter, value)
    }
  }
}
