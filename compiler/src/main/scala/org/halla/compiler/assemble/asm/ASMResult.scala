package org.halla.compiler.assemble.asm

sealed trait ASMResult

object ASMResult {
  type Compiled = Array[Byte]

  case class Success(namespace: Seq[String], name: String, code: Compiled) extends ASMResult
//  case class Failure(reason: String)
}
