package org.halla.compiler.assemble.asm.specs

import org.halla.compiler.assemble.asm.visit.Method

case class MethodSpecifications(name: String, args: Seq[Class[_]], body: Method)
