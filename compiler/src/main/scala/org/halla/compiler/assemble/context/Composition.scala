package org.halla.compiler.assemble.context

import org.halla.compiler.assemble.asm.BuilderContext
import org.halla.compiler.assemble.asm.visit.Method
import org.halla.compiler.assemble.context.Composition.{Composed, CompositionContext}
import org.halla.compiler.lambdify.lambda.Type
import org.objectweb.asm

sealed trait Composition[R] {
  def compose[A](consumer: R => A)(implicit context: CompositionContext): Composed[A]

  def map[A](f: R => Composition[A]): Composition[A]

  def mapContext(f: CompositionContext => CompositionContext): Composition[R]

  def eval[A](consumer: (R, CompositionContext) => A): A = {

    // create context that will be ran through the composition chain, which will inject all the
    // important information we need at the end of the assembly process
    val context: CompositionContext = CompositionContext(builderContext = BuilderContext.default, cases = Seq())

    // plant the final compose in this chain, which starts the evaluation process
    val composedResult: Composed[R] = this.compose(identity)(context)

    // use the result of the evaluation of the composition chain to construct the final result of
    // the composition and return it.
    consumer(composedResult.r, composedResult.context)
  }

}

object Composition {

  /**
    * Case class used to hold all metadata while building/evaluation a single [[Composition]]
    *
    * @param builderContext used to write asm
    * @param cases to lazily compose
    */
  case class CompositionContext( builderContext: BuilderContext,
                                 cases: Seq[CaseMatchToAssemble]
                               )

  /*
   *   Cases used during building/evaluation of a composition
   */

  case class Composed[R](r: R, context: CompositionContext)

  case class CaseMatchToAssemble(t: Type.TypeUnion.Declared, returnType: asm.Type, branches: Map[Type.Composite, Method], name: String, abstractMethod: Method)

  case class Basic[R](r: R, contextMutation: CompositionContext => CompositionContext = cc => cc) extends Composition[R] {

    override def compose[A](consumer: R => A)(implicit context: CompositionContext): Composed[A] = {
      Composed(consumer(r), contextMutation(context))
    }

    override def map[G](f: R => Composition[G]): Composition[G] = {
      f.apply(r).mapContext(contextMutation)
    }

    def mapContext(f: CompositionContext => CompositionContext): Composition[R] = this.copy(
      contextMutation = (cc: CompositionContext) => f(contextMutation(cc))
    )
  }

  private def collect[R](toCompose: Iterable[Composition[R]], composed: Seq[R]): Composition[Iterable[R]] = {
    toCompose.headOption
      .map(nextToCompose => {
        nextToCompose.map(result => {
          // add the composed argument to the current list of composed args,
          // then continue to recursively map the remaining compositions
          collect(toCompose.tail, composed.:+(result))
        })
      })
      .getOrElse({
        // all compositions have been combined into a single composition, so now the only thing left to do
        // is compose to get the final result
        Basic(composed)
      })
  }

  def collect[R](toCompose: Iterable[Composition[R]]): Composition[Iterable[R]] = {
    collect(toCompose, Seq())
  }


  import scala.language.implicitConversions

  implicit def continue[R](r: R): Composition[R] = {
    Basic(r)
  }

}
