package org.halla.compiler.assemble.asm

case class BuilderContext(asmVersion: Int, compilerPrefix: String, built: Seq[ASMResult]) {
  def add(result: ASMResult): BuilderContext = {
    this.copy(built = this.built.:+(result))
  }
}

object BuilderContext {
  lazy val default = BuilderContext(49, "halla", Seq())
  def fresh(): BuilderContext = { BuilderContext(49, "halla", Seq()) }
}