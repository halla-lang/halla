package org.halla.compiler.assemble.compose.method

import org.halla.compiler.assemble.blueprint.ValueBlueprint.Values
import org.halla.compiler.assemble.bp.TypeSpecifier
import org.halla.compiler.assemble.context.Composition
import org.objectweb.asm.Type

object ParameterComposer {

  /**
    * Turn list of parameters into a list of types.
    *
    * Mainly used by [[org.halla.compiler.assemble.compose.MethodComposer]] when creating the builder for a method.
    *
    * @param parameters to turn to types
    * @return
    */
  def compose(parameters: Values): Composition[Iterable[Type]] = {

    Composition.collect(parameters.map(parameter => {
      TypeSpecifier.getOrDefineType(parameter)
    }))

  }
}
