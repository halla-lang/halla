package org.halla.compiler.assemble.asm.visit

import jdk.internal.org.objectweb.asm.Opcodes
import jdk.internal.org.objectweb.asm.Opcodes._
import org.halla.compiler.assemble.asm.visit.ProviderChain.ProviderChain
import org.halla.compiler.assemble.asm.visit.StackConsumer.{Invoke, Keep, Return}
import org.halla.compiler.assemble.asm.visit.invoke.Invocation
import org.objectweb.asm.MethodVisitor

case class VisitationChain(providers: ProviderChain, consumedBy: StackConsumer)

object VisitationChain {

  case class Builder(method: Method) {

    /**
      * Generate visitations to represent the invocation of the provided [[Invocation]].
      *
      * This allows for the generation of JVM byte code that represents function invocations as well as basic math
      * operations and more.
      *
      * Natives like "+" can be invoked via classes like [[org.halla.compiler.assemble.asm.visit.invoke.native.Math.Addition]].
      *
      * @param target to visit
      * @return
      */
    def invoke(target: Invocation): ProviderChain.Builder[VisitationChain.Builder] = {
      ProviderChain.Builder[VisitationChain.Builder](method, (provides: ProviderChain) => {
        // the final chain
        val chainToAdd = VisitationChain(providers = provides, consumedBy = Invoke(target))

        val newMethod = method.appendToImplementation(chainToAdd)

        Builder(newMethod)
      })
    }

    def push(): ProviderChain.Builder[VisitationChain.Builder] = {
      ProviderChain.Builder[VisitationChain.Builder](method, (provides: ProviderChain) => {
        // the final chain
        val chainToAdd = VisitationChain(providers = provides, consumedBy = Keep)

        val newMethod = method.appendToImplementation(chainToAdd)

        Builder(newMethod)
      })
    }

    def ret(): Method = {
      // the final chain
      val finalChain = VisitationChain(providers = Seq(), consumedBy = Return(method.returns))

      method.appendToImplementation(finalChain)
    }
  }

  def visit(visitor: MethodVisitor)(visitationChain: VisitationChain): Unit = {

    visitationChain.consumedBy match {

      case Invoke(invoked) =>
        invoked.visit(visitor, visitationChain.providers)

      case Keep =>
        visitationChain.providers.foreach(_.visit(visitor))

      case Return(maybeType) =>
        maybeType match {
          case Some(returnType) =>
            // visit return statement for the method's expected return type
            visitor.visitInsn(returnType.getOpcode(IRETURN))

          case None =>
            // visit void return statement
            visitor.visitInsn(Opcodes.RETURN)
        }
    }
  }
}
