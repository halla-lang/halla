package org.halla.compiler.assemble.compose.method

import java.math.BigInteger
import java.security.MessageDigest

import org.halla.compiler.assemble.instruct.resolve.InstructionGraph.Directed

object MethodNameGenerator {

  private lazy val md: MessageDigest = MessageDigest.getInstance("MD5")

  /**
    * Create a method name for an algebraic type match. This name will be added to the subject type's interface
    * @param atm
    * @return
    */
  def methodName(atm: Directed.AlgebraicTypeMatch): String = {
    val hashCode = atm.hashCode().toString
    val digest = md.digest(hashCode.getBytes)
    val bigInt = new BigInteger(1, digest)
    val hashedString = bigInt.toString(Character.MAX_RADIX)
    hashedString
  }

}
