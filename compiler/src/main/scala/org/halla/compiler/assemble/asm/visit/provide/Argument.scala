package org.halla.compiler.assemble.asm.visit.provide

import org.halla.compiler.assemble.asm.visit.{Method, ProviderChain}
import org.objectweb.asm.{MethodVisitor, Type}

case class Argument(position: Int, argType: Type) extends StackProvider {
  val returnType: Type = argType

  lazy val loadCodeForType: Int = {
    import org.objectweb.asm.Opcodes._
    // get the opcode specific to whatever type of argument this is.
    // i.e. if this argument is of type Float, use FLOAD.
    argType.getOpcode(ILOAD)
  }


  def visit(mv: MethodVisitor): Unit = {
    mv.visitIntInsn(loadCodeForType, position)
  }
}

object Argument {
  trait Builder[R] extends StackProvider.Builder[R] {
    val method: Method

    def argument(position: Int): ProviderChain.Builder[R] = {

      val argType = try { method.parameters.toList(position) } catch {
        case _: IndexOutOfBoundsException =>
          throw new RuntimeException(s"Tried to visit argument at index $position, but none existed. Arguments were: [${method.parameters.mkString(",")}]")
      }

      val adjustedPosition = if (method.isStatic) {
        position
      } else {
        //  ALOAD 0 on non-static methods is `this`, not the first argument.
        position + 1
      }

      this.add(Argument(adjustedPosition, argType))
    }
  }
}
