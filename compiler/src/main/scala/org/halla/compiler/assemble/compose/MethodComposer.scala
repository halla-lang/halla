package org.halla.compiler.assemble.compose

import org.halla.compiler.assemble.asm.visit.invoke.interface.InterfaceInvocation
import org.halla.compiler.assemble.asm.visit.{Method, ProviderChain, VisitationChain}
import org.halla.compiler.assemble.blueprint.FunctionBlueprint
import org.halla.compiler.assemble.blueprint.ValueBlueprint.Values
import org.halla.compiler.assemble.bp.TypeSpecifier
import org.halla.compiler.assemble.compose.method.{InvocationComposer, MethodNameGenerator, ParameterComposer, RefComposer}
import org.halla.compiler.assemble.context.Composition
import org.halla.compiler.assemble.instruct.resolve.InstructionGraph
import org.halla.compiler.assemble.instruct.resolve.InstructionGraph.Directed.AlgebraicTypeMatch
import org.halla.compiler.assemble.instruct.resolve.InstructionGraph.{Application, Directed, Reference}

object MethodComposer {

  import Composition._

  def compose[R](function: FunctionBlueprint, composeStatic: Boolean = true): Composition[Method] = {
    function match {
      case FunctionBlueprint(name: String, graph: InstructionGraph, parameters: Values) =>
        // start building a named function for the blueprint
        val baseMethodBuilder = {
          if (composeStatic) {
            Method.build(name).static()
          } else {
            Method.build(name)
          }

        }

        // get types of parameters
        ParameterComposer.compose(parameters) map (parameterTypes => {
          // get the return type
          TypeSpecifier.getOrDefineType(graph.result) map (returnType => {

            // define a static method
            val visitationBuilder: VisitationChain.Builder = baseMethodBuilder
              // set parameters
              .params(parameterTypes)
              // set return type
              .returns(returnType)
              // start builder for method body
              .body()

            // use composer to compose body.
            MethodComposer.initialCompose(function, graph, parameters, visitationBuilder) map (composedBody => {
              // end last provider composition
              composedBody.end()
                // and create return statement
                .ret()
            })
          })
        })
    }
  }

  /**
   * This method is called when we are composing a [[VisitationChain]].
   *
   * This composer is currently only called when the stack is empty.
   *
   * @param function we are composing
   * @param graph
   * @param parameters
   * @param visitationBuilder
   * @return
   */
  def initialCompose(function: FunctionBlueprint, graph: InstructionGraph, parameters: Values, visitationBuilder: VisitationChain.Builder): Composition[ProviderChain.Builder[VisitationChain.Builder]] = {
    graph match {

      /** ********************************
       * Compose user defined graphs
       */

      // compose a visitation for an instruction
      case Reference(ref) =>
        // compose implicitly using the continuity
        RefComposer.compose(ref)(visitationBuilder.push())

      // compose visitation to realize the invocation of a function made up of multiple instructions
      case Application(symbol, applied, result) =>
        // compose the function that is being invoked
        InvocationComposer.compose(symbol)(visitationBuilder) map (composedApplication => {
          // after composing the function we will be invoking we will compose each of the arguments
          applied.foldRight(composedApplication)((graph: InstructionGraph, current: ProviderChain.Builder[VisitationChain.Builder]) => {
            sequentialCompose(function, graph, parameters, current)
          })
        })

      // assemble visitations for directed graph of instructions
      case d: Directed =>
        d match {

          case atm@AlgebraicTypeMatch(_, subject, subjectType, branches) =>
            // generate method name for match statement which will be added to top level type interface
            val matchStatementFunctionName = MethodNameGenerator.methodName(atm)

            // get the return type of the entire statement
            TypeSpecifier.getOrDefineType(d.result) map (returnType => {

              // TODO figure out a classpath to give types so they will be namespaced properly
              val invocation = InterfaceInvocation(s"halla/udt/${subjectType.symbol}", matchStatementFunctionName, Some(returnType))

              val abstractMatchMethod = Method.build(matchStatementFunctionName)
                .returns(returnType)
                .abstrct()

              // convert each branch to a writable method
              Composition.collect(branches.map({
                case (caseType, branchGraph) =>
                  // convert branch to function blueprint
                  val branchFunction = FunctionBlueprint(
                    name = matchStatementFunctionName,
                    graph = branchGraph,
                    parameters = List()
                  )(src = branchGraph.src)

                  // use blueprint and composer to compose method for branch
                  MethodComposer.compose(branchFunction, composeStatic = false) map {
                    caseType -> _
                  }
              })) map { composedBranches => {

                // compose subject and provide it to the match function
                sequentialCompose(function, subject, parameters, visitationBuilder.invoke(invocation))

                  // register the branches of this case with the subject type so that a function can be generated
                  .mapContext(composedContext => {

                    // this will be the case to assemble later
                    val caseMatchToAssemble = CaseMatchToAssemble(
                      t = subjectType,
                      returnType = returnType,
                      branches = composedBranches.toMap,
                      name = matchStatementFunctionName,
                      abstractMethod = abstractMatchMethod
                    )

                    // add the case to the composed context
                    composedContext.copy(cases = composedContext.cases :+ caseMatchToAssemble)
                  })
              }
              }
            })
        }
    }
  }

  /**
   * This compose method is called while we are building a [[ProviderChain]].
   *
   * This composer is currently only called while we are providing arguments to an invocation, which is why a lot of
   * stuff here lacks an implementation.
   *
   * @param function we are composing
   * @param graph
   * @param parameters
   * @param visitationBuilder
   * @return
   */
  def sequentialCompose(function: FunctionBlueprint, graph: InstructionGraph, parameters: Values, visitationBuilder: ProviderChain.Builder[VisitationChain.Builder]): ProviderChain.Builder[VisitationChain.Builder] = {
    graph match {

      /** ********************************
       * Compose user defined graphs
       */

      // compose a visitation that references a defined value
      case Reference(ref) =>
        RefComposer.compose(ref)(visitationBuilder)

      // compose visitation to realize the invocation of a function made up of multiple instructions
      case Application(symbol, applied, result) =>
        ???

      // assemble visitations for directed graph of instructions
      case d: Directed =>
        ???

    }
  }
}
