package org.halla.compiler.assemble.instruct

import org.halla.compiler.lambdify.lambda.Type.ConcreteType

/**
  * Used to infer how a statement modifies the stack
  *
  * @param consumes types being taken from the stack
  * @param produces types being added to the stack
  */
final case class StackModifier(consumes: Seq[ConcreteType], produces: Seq[ConcreteType]) {
  lazy val direction: Int = produces.length - consumes.length
}

object StackModifier {

  /**
    * Convenience constructor for [[StackModifier]]
    * @param types of objects being produced
    */
  def producing(types: ConcreteType*): StackModifier = {
    StackModifier(Seq(), types)
  }

  /**
   * Convenience constructor for [[StackModifier]]
   * @param singleType of objects being produced
   */
  def producing(singleType: ConcreteType): StackModifier = {
    StackModifier(Seq(), Seq(singleType))
  }

  import scala.language.implicitConversions
  implicit def toProducer(typeBlueprint: ConcreteType): StackModifier = producing(typeBlueprint)

  def asProducerOfOne(assertedProducer: StackModifier): ConcreteType = {
    assertedProducer match {
      case StackModifier(Seq(), Seq(someType)) =>
        someType
      case _ =>
        ???
    }
  }

  lazy val zero: StackModifier = StackModifier(Seq(), Seq())
}
