package org.halla.compiler.assemble.compose.method

import org.halla.compiler.assemble.asm.visit.ProviderChain

object LiteralComposer {
  def compose[R](builder: ProviderChain.Builder[R], any: Any): ProviderChain.Builder[R] = {
    any match {
      case i: Integer =>
        builder.integer(i)

      case s: String =>
        builder.string(s)

      case f: Float =>
        builder.float(f)

      case _ =>
        throw new RuntimeException(s"Could not find type for unsupported literal `$any` of type `${any.getClass.getCanonicalName}`. Currently only, Int, String and Float are supported.")
    }
  }
}
