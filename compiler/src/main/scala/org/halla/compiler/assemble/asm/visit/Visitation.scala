package org.halla.compiler.assemble.asm.visit

trait Visitation

object Visitation {

//  case class StaticMethodCall(owner: String, name: String, desc: String) extends Visitation {
//    def apply(mv: MethodVisitor): Unit = {
//      //      v.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", s"(${Type.getDescriptor(classOf[String])})V")
//      mv.visitMethodInsn(INVOKEVIRTUAL, owner, name, s"(${Type.getDescriptor(classOf[Integer])})V")
//    }
//  }
//
//  object Math {
//    case class Addition(typesAdded: Type) extends Visitation {
//      def apply(mv: MethodVisitor): Unit = {
//        mv.visitInsn(typesAdded.getOpcode(IADD))
//      }
//    }
//  }
//
//  case class Return(returnType: Type) extends Visitation {
//    def apply(mv: MethodVisitor): Unit = {
//      mv.visitInsn(returnType.getOpcode(IRETURN))
//    }
//  }

  trait StackItemSource
  case class StackProvider(source: StackItemSource)
  case class StackConsumer() extends Visitation
}
