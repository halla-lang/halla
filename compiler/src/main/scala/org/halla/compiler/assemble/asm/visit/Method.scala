package org.halla.compiler.assemble.asm.visit

import org.halla.compiler.assemble.asm.visit.Method.{Abstract, Implementation}
import org.objectweb.asm.Type

import scala.reflect.ClassTag

case class Method(name: String, parameters: Seq[Type], returns: Option[Type], isStatic: Boolean, body: Method.Body) {
  def appendToImplementation(visitation: VisitationChain): Method = {
    this.body match {
      case Implementation(chains) =>
        this.copy(body = Implementation(chains.:+(visitation)))
      case Abstract =>
        this.copy(body = Implementation(Seq(visitation)))
    }
  }
}

object Method {

  sealed trait Body
  case class Implementation(chains: Seq[VisitationChain]) extends Body
  case object Abstract extends Body

  case class DeclarationBuilder(method: Method) {
    def static(): DeclarationBuilder = {
      this.copy(method = method.copy(isStatic = true))
    }

    def returns[T: ClassTag]: DeclarationBuilder = {
      import scala.reflect._
      val clazz = classTag[T].runtimeClass
      returns(Type.getType(clazz))
    }

    def returns(returnType: Type): DeclarationBuilder = {
      this.copy(method = method.copy(returns = Some(returnType)))
    }

    def params(params: Iterable[Type]): DeclarationBuilder = {
      this.copy(method = method.copy(parameters = params.toSeq))
    }

    def body(): VisitationChain.Builder = {
      VisitationChain.Builder(method)
    }

    def abstrct(): Method = {
      method.copy(body = Abstract)
    }
  }

  def build(name: String): DeclarationBuilder = {
    val method = Method(name, parameters = Seq(), returns = None, isStatic = false, body = Abstract)
    DeclarationBuilder(method)
  }
}
