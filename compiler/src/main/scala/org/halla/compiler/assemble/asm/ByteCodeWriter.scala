package org.halla.compiler.assemble.asm

import org.halla.compiler.assemble.asm.ASMResult.Success

object ByteCodeWriter {

  def write(outputDir: String, compiled: ASMResult): Unit = {
    import java.io.FileOutputStream

    compiled match {
      case Success(namespace, name, code) =>
        // create output stream to write bytecode to
        val out = new FileOutputStream(s"$outputDir/${namespace.mkString("/")}/$name.class")

        // write the byte code
        out.write(code)
    }


  }

}
