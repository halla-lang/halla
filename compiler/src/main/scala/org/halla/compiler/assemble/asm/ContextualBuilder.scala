package org.halla.compiler.assemble.asm

import com.typesafe.scalalogging.LazyLogging
import org.halla.compiler.assemble.asm.ASMResult.Success
import org.halla.compiler.assemble.asm.specs.ClassSpecifications
import org.halla.compiler.assemble.asm.specs.ClassSpecifications.{Clazz, Interface}
import org.halla.compiler.assemble.asm.visit.Method.{Abstract, Implementation}
import org.halla.compiler.assemble.asm.visit.VisitationChain
import org.objectweb.asm.ClassWriter

object ContextualBuilder extends LazyLogging {

  def defineClazz(specs: ClassSpecifications)(implicit builderContext: BuilderContext): ASMResult = {

    import org.objectweb.asm.Opcodes._

    // create class writer
    val cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES)

//    val ocw = new ClassWriter(ClassWriter.COMPUTE_FRAMES)
//    val cw: TraceClassVisitor = new TraceClassVisitor(ocw, new PrintWriter(System.out))

    // set up class writer based on what kind of class we are defining
    val methodDefinitions = specs match {
      case c@Clazz(_, _, implements, defaultConstructor, methods) =>
        cw.visit(builderContext.asmVersion,
          ACC_PUBLIC + ACC_SUPER,
          c.computePath,
          null,
          "java/lang/Object",
          implements.map(_.computePath).toArray
        )

        // starts write process
        cw.visitSource(s"${specs.className}.java", null)

        // if a default constructor was requested
        if (defaultConstructor) {
          // build default constructor for class
          val mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null)

          mv.visitVarInsn(ALOAD, 0)
          mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V")
          mv.visitInsn(RETURN)
          mv.visitMaxs(0,0)
          mv.visitEnd()
        }

        methods

      case i@Interface(_, _, methods) =>
        cw.visit(builderContext.asmVersion,
          ACC_PUBLIC + ACC_ABSTRACT + ACC_INTERFACE,
          i.computePath,
          null,
          "java/lang/Object",
          Array()
        )

        // starts write process
        cw.visitSource(s"${specs.className}.java", null)

        methods
    }

    // define methods
    methodDefinitions.foreach(method => {

      val staticFlagValue = if (method.isStatic) { ACC_STATIC } else { 0 }

      method.body match {
        case Implementation(visitations) =>
          // build method signature
          val mv = cw.visitMethod(
            ACC_PUBLIC + staticFlagValue,
            method.name,
            s"(${method.parameters.mkString})${method.returns.map(_.getDescriptor).getOrElse("V")}",
            null,
            null
          )

          // visit each chain, this will write the ASM representing the function body
          visitations.foreach(VisitationChain.visit(mv))

          // still need to call visitMaxes, even if they are computed
          mv.visitMaxs(1,1)
          mv.visitEnd()

      case Abstract =>
        // build method signature
        val mv = cw.visitMethod(
          ACC_PUBLIC + ACC_ABSTRACT,
          method.name,
          s"(${method.parameters.mkString})${method.returns.map(_.getDescriptor).getOrElse("V")}",
          null,
          null
        )

        mv.visitEnd()

      }

    })

    // get output
    Success(specs.nameSpace, specs.className, cw.toByteArray)

//    // show output
//    cw.p.print(new PrintWriter(System.out))
//
//    val lines = getLines(cw.p.getText)
//    logger.debug(s"Code Written:\n>>>>>> start <<<<<<\n${new String(lines.mkString)}\n>>>>>> end <<<<<<")
//    Success(specs.nameSpace, specs.className, Array())
  }

  def getLines(codeChunks: java.util.List[_]): Seq[String] = {
    import scala.jdk.CollectionConverters._

    codeChunks.asScala.toSeq.flatMap {
      case line: String =>
        Seq(line)

      case lines: java.util.List[_] =>
        getLines(lines)
    }
  }
}
