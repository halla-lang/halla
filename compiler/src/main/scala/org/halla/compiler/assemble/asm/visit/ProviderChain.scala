package org.halla.compiler.assemble.asm.visit

import org.halla.compiler.assemble.asm.visit.provide.{Argument, Literal, StackProvider}

object ProviderChain {

  type ProviderChain = Seq[StackProvider]

  case class Builder[R](method: Method, build: ProviderChain => R, providers: Seq[StackProvider] = Seq())
    extends Literal.Builder[R]
    with Argument.Builder[R]
  {

    protected override def add(stackProvider: StackProvider): Builder[R] = {
      this.copy(providers = providers.:+(stackProvider))
    }

    override def end(): R = {
      build(providers)
    }
  }
}
