package org.halla.compiler.parse.failures

import org.halla.compiler.common.CompilationError
import org.halla.compiler.parse.Source
import org.halla.compiler.parse.parsed.Parsed
import parseback.{Line, ParseError}

sealed trait ParserError extends CompilationError {

  // TODO remove src from CompilationError since it does not apply to Parsed
  override def src: Seq[Parsed] = Seq()
}

object ParserError {

  def from(parsebackErr: ParseError, input: Source): ParserError = {
    parsebackErr match {
      case _: ParseError.WithLoc =>
        ParsingErr(input, parsebackErr)

      case _: ParseError.Meta =>
        Meta(input)

      case ParseError.UnexpectedEOF(_) =>
        ParsingErr(input, parsebackErr)

    }
  }

  case class EmptySource(source: Source) extends ParserError {

    /**
     * @return explanation of the error.
     */
    override def explanation: String = "A file was empty."
  }

  case class Meta(source: Source) extends ParserError {

    /**
     * @return explanation of the error.
     */
    override def explanation: String = "Parser encountered an unrecoverable failure. This is likely an issue with Halla's compiler itself and not your code."
  }

  case class ParsingErr(source: Source, parsebackErr: ParseError) extends ParserError {

    /**
     * @return explanation of the error.
     */
    override def explanation: String = parsebackErr.render(source.toString)
  }

  case class IOException(source: Source, err: Throwable) extends ParserError {

    /**
     * @return explanation of the error.
     */
    override def explanation: String = "Encountered an unexpected <EOF> while parsing the source code."
  }

  case class UnexpectedEOF(source: Source) extends ParserError {

    /**
     * @return explanation of the error.
     */
    override def explanation: String = "Encountered an unexpected <EOF> while parsing the source code."
  }

  case class EmptyBody(source: List[Line]) extends ParserError {
    /**
     * @return explanation of the error.
     */
    override def explanation: String = "All functions must contain an expression in their body."
  }

  case class LastElementOfBodyMustBeExpression(source: List[Line]) extends ParserError {
    /**
     * @return explanation of the error.
     */
    override def explanation: String = "The last element of a function's body must always be an expression."
  }

}
