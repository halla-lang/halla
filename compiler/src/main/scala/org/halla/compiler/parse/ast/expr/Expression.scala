package org.halla.compiler.parse.ast.expr

import cats.data.{Chain, NonEmptyChain, NonEmptyList}
import org.halla.compiler.parse.ast.expr.Expression.Match.MatchCase
import org.halla.compiler.parse.ast.typedef.TypeRef
import org.halla.compiler.parse.parsed.NS.NameSpace
import org.halla.compiler.parse.parsed.{NS, Parsed}

sealed trait Expression extends Parsed

object Expression {

  import org.halla.compiler.parse._
  import parseback._

  /**
   * A simple expression is one that contains a single control flow, meaning it has a single entrypoint and exit. i.e. it has a single return
   *
   * Examples:
   * - ` 1 + 1 `
   * - ` "yolo" `
   * - ` 42 `
   */
  sealed trait SimpleExpression extends Expression

  /**
   * Accessing the attribute of an object.
   *
   * @param accessed expression
   * @param property that is being accessed
   * @param loc      src
   */
  final case class Access(accessed: Ref, property: Ref)(val loc: List[Line]) extends SimpleExpression

  object Access {

    import Ref._

    implicit val access: Parser[Access] =
      (ref ~ "." ~ ref) ^^ { (loc, accessed, _, property) => Access(accessed, property)(loc = loc) }
  }

  /**
   * Using one expression as an argument to another.
   *
   * @param left  expression
   * @param right expression
   * @param loc   src
   */
  final case class Application(left: SimpleExpression, right: SimpleExpression)(val loc: List[Line]) extends SimpleExpression

  object Application {
    implicit val W: Whitespace = Whitespace(spaceOrTab)

    implicit val application: Parser[Application] =
      simpleExpression ~ simpleExpression ^^ { (loc, expr1, expr2) => Application(left = expr1, right = expr2)(loc = loc) }

    implicit val operation: Parser[Application] = {
      import Ref._

      simpleExpression ~ operator ~ simpleExpression ^^ { (loc, expr1, op, expr2) => {
        val firstApplication = Application(left = op, right = expr1)(expr1.loc ++ op.loc)
        Application(left = firstApplication, right = expr2)(loc)
      }
      }
    }
  }

  /**
   * Float literal
   *
   * @param value of the float
   * @param loc   src
   */
  final case class FloatLiteral(value: Float)(val loc: List[Line]) extends SimpleExpression

  object FloatLiteral {

    import parseback._

    implicit val float: Parser[FloatLiteral] = {
      integer ~ "." ~ integer ^^ { (loc, number, _, decimal) => FloatLiteral(value = s"$number.$decimal".toFloat)(loc = loc) }
    }
  }

  /**
   * Integer literal
   *
   * @param value of the integer
   * @param loc   src
   */
  final case class IntegerLiteral(value: Int)(val loc: List[Line]) extends SimpleExpression

  object IntegerLiteral {

    import parseback._

    implicit val int: Parser[IntegerLiteral] = {
      """\d+""".r ^^ { (loc, int) => IntegerLiteral(value = Integer.parseInt(int))(loc = loc) }
    }
  }

  /**
   * String literal
   *
   * @param value of the string
   * @param loc   src
   */
  final case class StringLiteral(value: String)(val loc: List[Line]) extends SimpleExpression

  object StringLiteral {

    import parseback._

    implicit val string: Parser[StringLiteral] = {
      "\"" ~ """(\\.|[^"\\])+""".r ~ "\"" ^^ { (loc, _, stringBody, _) =>
        StringLiteral(value = StringContext.processEscapes(stringBody))(loc = loc)
      }
    }
  }

  /**
   * A Reference to some other value.
   *
   * @param symbol of the target that the ref refers to
   * @param loc    src
   */
  final case class Ref(symbol: String)(val loc: List[Line]) extends SimpleExpression

  object Ref {

    import org.halla.compiler.parse._
    import parseback._

    /**
     * Captures a reference like: someVariable
     */
    implicit val ref: Parser[Ref] =
      name ^^ { (loc, name) => new Ref(symbol = name)(loc = loc) }

    /**
     * Captures a list of references like: someVar someVar1 ...
     */
    implicit val refs: Parser[Chain[Ref]] = {
      implicit val W: Whitespace = Whitespace("""\s+""".r)

      (ref ^^ { (loc, tv) => Chain(tv) }
        | ref.* ^^ { (loc, refs) => Chain.fromSeq(refs) }
        )
    }

    implicit val operator: Parser[Ref] = {
      def parserForOperator(symbol: String): Parser[Ref] = symbol ^^ { (loc, _) => new Ref(symbol)(loc) }

      parserForOperator("++") | parserForOperator("+") | parserForOperator("-") | parserForOperator("*") | parserForOperator("/")
    }
  }

  /**
   * A more complex expression that can have more than one point of return. examples are function bodies with `if` statements, `match` statements or `let` statements
   */
  sealed trait ControlExpression extends Expression

  /**
   * Pattern Match statement.
   *
   * @param subject of the pattern match
   * @param cases   to match subject against
   * @param loc     src
   */
  final case class Match(subject: SimpleExpression, cases: NonEmptyList[MatchCase])(val loc: List[Line]) extends ControlExpression

  object Match {

    /**
     * A match case for a given match statement. This could be anything from a static value like "yolo" to a case class
     * match like the [[CaseConstructorMatch]].
     */
    sealed trait MatchCase extends Parsed {
      def expression: Expression

      def namespace: NameSpace
    }

    /**
     * A match case that matches when the subject is an instance of some match case class.
     *
     * @param target     type of case class that's constructor is being referenced
     * @param arguments  for the case class that will be accessible like variables in the provided expression
     * @param expression that will be run if the case matches
     * @param namespace  to use for indexing symbols and linking references
     * @param loc        src
     */
    final case class CaseConstructorMatch(target: TypeRef, arguments: List[Ref])(val expression: Expression, val namespace: NameSpace, val loc: List[Line]) extends MatchCase

    import Ref._
    import TypeRef._
    import org.halla.compiler.parse._
    import parseback._

    implicit val W: Whitespace = Whitespace(anySpace)

    def createNamespacedMatchCase(target: TypeRef, arguments: List[Ref], expression: NS[Expression], loc: List[Line]): NS[CaseConstructorMatch] = {
      NS.apply(namespace => {
        val matchCaseNamespace = {
          // ccm stands for Case Constructor Match
          val matchCasePathPart = "$ccm$" + target.name
          // append the new path part to the current namespace, wherever that is.
          namespace.append(matchCasePathPart)
        }
        CaseConstructorMatch(target = target, arguments = arguments.toList)(expression = expression.unwrap(matchCaseNamespace), namespace = namespace, loc = loc)
      })
    }

    implicit val matchCase: Parser[NS[CaseConstructorMatch]] = {
      (typeReferenceWithoutArgs ~ ref.* ~ ("->" ~> expression) ^^ { (loc, target, args, expr) => createNamespacedMatchCase(target, args, expr, loc) }
        | (typeReference ~ ("->" ~> expression) ^^ { (loc, target, expr) => createNamespacedMatchCase(target, List.empty, expr, loc) })
        )
    }

    implicit val matchCases: Parser[NonEmptyChain[NS[CaseConstructorMatch]]] = {
      (matchCase ^^ { (loc, tv) => NonEmptyChain(tv) }
        | matchCase ~ matchCases ^^ { (loc, some, more) => more.+:(some) }
        )
    }

    implicit lazy val matchStatement: Parser[NS[Match]] = {
      simpleExpression ~ ("match" ~> matchCases) ^^ { (loc, subject, cases) => {
        NS(namespace => {
          Match(subject = subject, cases = cases.map(namespacedCase => namespacedCase.unwrap(namespace)).toNonEmptyList)(loc = loc)
        })
      }
      }
    }

  }

  implicit val simpleExpression: Parser[SimpleExpression] = {

    import Access._
    import Application._
    import FloatLiteral._
    import IntegerLiteral._
    import Ref._
    import StringLiteral._

    operation | application | access | operator | float | int | string | ref
  }

  implicit val simpleExpressions: Parser[Seq[SimpleExpression]] = {
    (simpleExpression ^^ { (loc, se) => Seq(se) }
      | simpleExpression ~ simpleExpressions ^^ { (loc, some, more) => more.+:(some) }
      )
  }

  implicit lazy val controlExpression: Parser[NS[ControlExpression]] = {
    import Match._

    matchStatement
  }

  implicit val expression: Parser[NS[Expression]] = {
    controlExpression | simpleExpression.map(NS.agnostic)
  }
}
