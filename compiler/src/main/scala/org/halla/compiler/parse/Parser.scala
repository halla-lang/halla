package org.halla.compiler.parse

import org.halla.compiler.common
import org.halla.compiler.common.HResult.{Failure, Success}
import org.halla.compiler.common.{CompilationStep, HResult}
import org.halla.compiler.parse.ast.Package
import org.halla.compiler.parse.failures.ParserError
import org.halla.compiler.parse.failures.ParserError.EmptySource
import parseback.util.Catenable
import parseback.util.Catenable.{Append, Empty, Single}


/**
  * The [[Parser]] parses halla source code into an AST.
  */
object Parser extends CompilationStep[Source, Package] {

  import parseback._

  /**
   * Parse the code found in the provided input.
   *
   * @param input to the Parser
   * @return Ongoing [[HResult]] to be used in later compilation steps.
   */
  override def run(input: Source): common.HResult[Package] = {

    for {
      lines <- SourceReader.read(input)
    } yield {

      /**
       * Recursively parse the source code.
       *
       * @param cat parseback io wrapper for the parsing process
       * @return
       */
      @scala.annotation.tailrec
      def matchCat(cat: Catenable[Package]): HResult[Package] = {
        cat match {
          case Single(actualResult) =>
            Success(actualResult)
          case Append(l, _) =>
            matchCat(l.apply())
          case Empty =>
            Failure(EmptySource(input))
        }
      }

      Package.`package`(lines).value match {
        case Right(cat) =>
          return matchCat(cat)

        case Left(errs: Seq[ParseError]) =>
          errs.map(ParserError.from(_, input)) match {
            case Nil =>
              return Failure(ParserError.Meta(input))
            case ::(head, tl) =>
              return Failure(head, tl: _*)
          }
      }
    }
  }
}
