package org.halla.compiler.parse.ast.imprt

import org.halla.compiler.parse.parsed.Parsed
import parseback.Line

final case class Import(namespace: Seq[String], name: String)(val loc: List[Line]) extends Parsed

object Import {

  import org.halla.compiler.parse.ast.Package._
  import org.halla.compiler.parse.parsed.NS.NameSpace.namespace
  import parseback._

  implicit val importDefinition: Parser[Import] = {
    implicit val W: Whitespace = Whitespace("""\s+""".r)
    """^import""".r ~> namespace ^^ { (loc, ns) => Import(namespace = ns.tail, name = ns.head)(loc = loc) }
  }


  implicit val importDefinitions: Parser[Seq[Import]] = {
    import org.halla.compiler.parse._

    ( importDefinition ^^ { (loc, imp) => Seq(imp)}
    | importDefinition ~ anySpace ~ importDefinitions ^^ { (loc, some, _, more) => more.+:(some) }
    )
  }

}
