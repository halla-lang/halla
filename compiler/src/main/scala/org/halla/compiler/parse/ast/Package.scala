package org.halla.compiler.parse.ast

import cats.data.Chain
import org.halla.compiler.parse.ast.func.Function
import org.halla.compiler.parse.ast.imprt.Import
import org.halla.compiler.parse.ast.typedef.TypeDefinition
import org.halla.compiler.parse.parsed.NS.NameSpace
import org.halla.compiler.parse.parsed.{NS, Parsed}
import parseback.Line

final case class Package(namespace: NameSpace, imports: Seq[Import], types: Seq[TypeDefinition], functions: Seq[Function])(val loc: List[Line]) extends Parsed {
  lazy val adts: Chain[TypeDefinition] = Chain.fromSeq(
    types.flatMap(d => d match {
      case td: TypeDefinition => Some(td)
      case _ => None
    })
  )

  lazy val functionDefinitions: Chain[Function] = Chain.fromSeq(
    functions.flatMap(d => d match {
      case td: Function => Some(td)
      case _ => None
    })
  )
}

object Package {

  import NS.NameSpace.namespace
  import org.halla.compiler.parse._
  import parseback._

  implicit val packageHeader: Parser[Package] = {
    implicit val W: Whitespace = Whitespace(spaceOrTab)

    """^package""".r ~> namespace ^^ { (loc, namespace) =>

      Package(namespace = namespace, imports = Seq(), types = Seq(), functions = Seq())(loc = loc)
    }
  }

  implicit val definitions: Parser[(List[NS[TypeDefinition]], List[NS[Function]])] = {

    import Function._
    import TypeDefinition._

    type SomeDef = Either[NS[TypeDefinition], NS[Function]]

    lazy val aDef: Parser[SomeDef] = {
      (typeDefinition ^^ { (loc, adt) => Left(adt) }
      | function ^^ { (loc, f) => Right(f) }
      )
    }

    lazy val someDefs: Parser[List[SomeDef]] = {
      (aDef ^^ { (loc, definition) => List(definition) }
      | aDef ~ anySpace ~ someDefs ^^ { (loc, some, _, more) => more.+:(some) }
      )
    }.?.map[List[SomeDef]](maybeSomeDefs => {
      maybeSomeDefs.getOrElse(List.empty[SomeDef])
    })

    someDefs.map(_.foldRight((List[NS[TypeDefinition]](), List[NS[Function]]()))({ case (definition, separated) =>
      definition match {
        case Left(adt) =>
          val namespacedDefs: List[NS[TypeDefinition]] = separated._1
          (namespacedDefs :+ adt, separated._2)
        case Right(f) =>
          (separated._1, separated._2 :+ f)
      }
    }))
  }

  lazy val `package`: Parser[Package] = {
    import org.halla.compiler.parse.ast.imprt.Import._


    (packageHeader <~ anySpace) ~ importDefinition.* ~ definitions ^^ { (loc, `package`, imports, types, functions) =>
      `package`.copy(imports = imports, types = types.map(namespacedTypes => {
        namespacedTypes.unwrap(`package`.namespace)
      }), functions = functions.map(namespacedFunctions => {
        namespacedFunctions.unwrap(`package`.namespace)
      }))(loc)
    }
  }

}