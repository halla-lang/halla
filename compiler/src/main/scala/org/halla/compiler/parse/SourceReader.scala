package org.halla.compiler.parse

import cats.Eval
import org.halla.compiler.common.HResult
import org.halla.compiler.common.HResult.{Failure, Success}
import org.halla.compiler.parse.failures.ParserError
import parseback.LineStream

object SourceReader {

  import java.io._

  import cats.effect.{IO, Resource}

  type Lines = LineStream[Eval]

  /**
   * Get a reader instance for some [[Source]].
   *
   * @param src source to read
   * @return
   */
  private def readerInstance(src: Source): Resource[IO, BufferedReader] = {
    val reader: Reader = src match {
      case Source.Programmatic(source) =>
        new StringReader(source)

      case Source.FromFile(file) =>
        new InputStreamReader(new FileInputStream(file))
    }

    Resource.fromAutoCloseable(IO(new BufferedReader(reader)))
  }

  /**
   * Read each line of the source.
   *
   * @param src to read
   * @return
   */
  def read(src: Source): HResult[Lines] = {

    readerInstance(src).use[Lines](srcReader => {
      import scala.jdk.CollectionConverters._

      IO({
        // read each line of the source
        val lines = srcReader.lines().iterator()
          // convert to Lines
          .asScala.toList

        LineStream[Eval](str = lines.mkString("\n"))
      })

    }).attempt.unsafeRunSync() match {
      case Left(err) =>
        Failure(ParserError.IOException(src, err))

      case Right(value) =>
        Success(value)
    }
  }
}
