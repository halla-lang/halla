package org.halla.compiler.parse.ast.typedef

import org.halla.compiler.parse.parsed.NS.NameSpace
import org.halla.compiler.parse.parsed.NS.NameSpace.NativeSpace
import org.halla.compiler.parse.parsed.{NS, Parsed}
import parseback.Line

/**
 * A Parsed Type
 */
sealed trait TypeDefinition extends Parsed {
  def nameSpace: NameSpace

  def symbol: String
}

object TypeDefinition {

  /**
   * Type Definitions
   */

  case class ADT(symbol: String, genericArguments: List[GenericArgument], variations: Seq[TypeRef], nameSpace: NameSpace)(val loc: List[Line]) extends TypeDefinition

  case class CaseDefinition(symbol: String, typeArguments: Seq[TypeRef], nameSpace: NameSpace)(val loc: List[Line]) extends TypeDefinition

  /**
   * Native Type Definitions
   */

  case object IntDefinition extends TypeDefinition {
    override def symbol: String = "Int"

    override def loc: List[Line] = List.empty

    override def nameSpace: NameSpace = NativeSpace
  }

  case object FloatDefinition extends TypeDefinition {
    override def symbol: String = "Float"

    override def loc: List[Line] = List.empty

    override def nameSpace: NameSpace = NativeSpace
  }

  case object StringDefinition extends TypeDefinition {
    override def symbol: String = "String"

    override def loc: List[Line] = List.empty

    override def nameSpace: NameSpace = NativeSpace
  }

  /**
   * Parsing
   */

  import org.halla.compiler.parse._
  import parseback._

  lazy val caseTypeDefinition: Parser[NS[CaseDefinition]] = {
    import TypeRef._

    implicit val W: Whitespace = Whitespace(anySpace)

    ("^case".r ~> typeReference).map(typeRef => {
      NS(path => CaseDefinition(typeRef.name, typeRef.typeArguments, path)(typeRef.loc))
    })
  }

  private lazy val references: Parser[Seq[TypeRef]] = {
    import TypeRef._

    implicit val W: Whitespace = Whitespace(anySpace)

    ( typeReference ^^ { (loc, ref) => Seq(ref)}
    | (typeReference <~ "|") ~ references ^^ { (loc, one, many) => many.+:(one) }
    )
  }


  lazy val declaration: Parser[(String, List[GenericArgument])] = {
    import GenericArgument._
    import org.halla.compiler.parse._

    // declaration of a non-generic ADT
    (classCaseString ^^ { (loc, baseRef) => (baseRef, List.empty) }
    // declaration of a generic ADT (i.e. "Maybe[A], ")
    | classCaseString ~ ("[" ~> genericArgument.* <~ "]") ^^ { (loc, baseRef, genericArguments) => (baseRef, genericArguments) }
    )
  }

  lazy val adt: Parser[NS[ADT]] = {
    {
      implicit val W: Whitespace = Whitespace(spaceOrTab)
      ("^type".r ~> declaration <~ "=") ~ references ^^ { (loc, baseRef, genericArguments, refs) => NS(path => ADT(symbol = baseRef, genericArguments = genericArguments, variations = refs, path)(loc = loc)) }
    }
  }

  lazy val typeDefinition: Parser[NS[TypeDefinition]] = {
    adt | caseTypeDefinition
  }

}