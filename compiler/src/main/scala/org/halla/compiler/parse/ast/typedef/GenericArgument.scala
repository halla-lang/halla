package org.halla.compiler.parse.ast.typedef

import parseback.Line

case class GenericArgument(name: String)(val loc: List[Line])

object GenericArgument {

  import org.halla.compiler.parse._
  import parseback._

  lazy val genericArgument: Parser[GenericArgument] = {
    name ^^ { (loc, name) => GenericArgument(name = name)(loc = loc) }
  }

  lazy val genericArguments: Parser[List[GenericArgument]] = {
    implicit val whitespace: Whitespace = spacesOrTabs

    ( genericArgument ^^ { (loc, arg) => List(arg) }
    | (genericArgument <~ ",") ~ genericArguments ^^ { (loc, head, tail) => head +: tail }
    )

  }

}