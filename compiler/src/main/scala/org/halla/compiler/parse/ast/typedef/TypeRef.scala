package org.halla.compiler.parse.ast.typedef

import org.halla.compiler.parse.parsed.Parsed
import parseback.Line

import scala.language.postfixOps

/**
 * Reference of a type
 *
 * @param name          of the type
 * @param typeArguments arguments for the type
 * @param loc           src
 */
case class TypeRef(name: String, genericArguments: List[GenericArgument], typeArguments: List[TypeRef])(val loc: List[Line]) extends Parsed

object TypeRef {

  import GenericArgument._
  import org.halla.compiler.parse._
  import parseback._

  lazy val typeReferenceWithoutArgs: Parser[TypeRef] = {

    (name ^^ { (loc, name) => TypeRef(name = name, genericArguments = List.empty, typeArguments = List.empty)(loc = loc) }
      | name ~ ("[" ~> genericArguments <~ "]") ^^ { (loc, name, genericArgs) => TypeRef(name = name, genericArguments = genericArgs, typeArguments = List.empty)(loc = loc) }
      )

  }

  lazy val typeReference: Parser[TypeRef] = {

    implicit val W: Whitespace = Whitespace(spaceOrTab)

    (typeReferenceWithoutArgs ^^ { (loc, typeRef) => typeRef }
    | (typeReference ~ typeReference.+) ^^ { (loc, baseType, args) => baseType.copy(typeArguments = args)(loc = loc) }
    )

  }

  lazy val typeReferences: Parser[List[TypeRef]] = typeReference.*
}
