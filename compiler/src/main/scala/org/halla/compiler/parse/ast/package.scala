package org.halla.compiler

import scala.util.matching.Regex

/**
 * Commonly used parsing regexs
 */
package object parse {

  import parseback._

  implicit val classCaseString: Regex = """([A-Z][a-zA-Z0-9]+)+""".r
  implicit val camelCaseString: Regex = """([a-z][a-zA-Z0-9]+)+""".r

  implicit val name: Regex = """[a-zA-Z]+""".r

  implicit val integer: Regex = """\d+""".r

  val spacesOrTabs: Whitespace = Whitespace(
    ( " " ^^ { (loc, littleSpace) => littleSpace }
    | "\t" ^^ { (loc, littleSpace) => littleSpace }
    ).*
  )

  implicit val spaceOrTab: Parser[String] =
    ("""[\s\t]+""".r ^^ { (loc, littleSpace) => littleSpace }
    )

  implicit val anySpace: Parser[String] =
    ("""\s""".r ^^ { (loc, ws) => ws }
    | """\t""".r ^^ { (loc, ws) => ws }
    | """\r""".r ^^ { (loc, ws) => ws }
    | """\n""".r ^^ { (loc, ws) => ws }
    ).*.map(_ => "")

}