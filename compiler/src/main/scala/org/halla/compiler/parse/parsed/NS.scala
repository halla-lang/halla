package org.halla.compiler.parse.parsed

import cats.{Applicative, MonoidK}
import cats.data.Chain
import cats.kernel.Monoid
import org.halla.compiler.parse.parsed.NS.NameSpace
import org.halla.compiler.parse.parsed.NS.NameSpace.NameSpaceImpl
import parseback.Parser

import scala.language.implicitConversions

/**
 * Short for "Name Spaced". This class allows for definitions to be namespaced by delaying their creation until a
 * Package's namespace has been successfully parsed.
 *
 * This is key for insuring that member's of the AST that share the same symbol can happily coexist, as long as authors
 * are careful with what they import into their scope.
 *
 * @param constructor for T that
 */
case class NS[+T](constructor: NameSpace => T) {
  def unwrap(namespace: NameSpace): T = {
    constructor.apply(namespace)
  }
}

object NS {

  def agnostic[A](a: A): NS[A] = NS(_ => a)

  implicit val applicativeNS = new Applicative[NS] {
    override def pure[A](x: A): NS[A] = NS.agnostic(x)

    override def ap[A, B](ff: NS[A => B])(fa: NS[A]): NS[B] = NS(namespace => {
      // unwrap a
      val a = fa.unwrap(namespace)
      // apply a to function
      ff.unwrap(namespace).apply(a)
    })
  }

  sealed trait NameSpace {
    def path: Seq[String]

    // all elements of the namespace except the head.
    def tail: Seq[String]

    // top level of the namespace
    def head: String

    def append(pathPart: String): NameSpace = {
      NameSpaceImpl(path :+ pathPart)
    }
  }

  object NameSpace {

    lazy val namespace: Parser[NameSpace] = {
      import org.halla.compiler.parse._
      import parseback._
      implicit val whitespace: Whitespace = Whitespace.Default

      ((name <~ ".".?).* ^^ { (loc, names) => NameSpace(names) }
      )
    }

    protected[compiler] case object NativeSpace extends NameSpace {
      override def path: Seq[String] = Seq("org", "halla", "$native")

      lazy val tail: Seq[String] = Seq("org", "halla")

      override def head: String = "$native$"
    }

    private case class NameSpaceImpl(path: Seq[String]) extends NameSpace {
      lazy val memoizedPath: Seq[String] = path.reverse

      override def tail: Seq[String] = memoizedPath.tail

      override def head: String = memoizedPath.head
    }

    def apply(path: Seq[String]): NameSpace = {
      NameSpaceImpl(path)
    }
  }

}