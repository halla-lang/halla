package org.halla.compiler.parse.parsed

import parseback.Line

trait Parsed {
  def loc: List[Line]
}

object Parsed {

  trait SrcMapped extends Parsed {
    def src: Seq[Parsed]
    lazy val loc: List[Line] = src.flatMap(_.loc).toList
  }

  case class MappedToSource(src: Seq[Parsed]) extends SrcMapped

  import scala.language.implicitConversions
  implicit def parsedTogether(parsed: Seq[Parsed]): SrcMapped = {
    MappedToSource(parsed)
  }

  implicit def parsedTogether(parsed: Parsed): Seq[Parsed] = {
    Seq(parsed)
  }
}
