package org.halla.compiler.parse

import java.io.File

sealed trait Source

object Source {

  /**
   * Source code as a [[String]]. This should really only be used for testing purposes, as it does not allow for helpful
   * error messages when dealing with large amounts of source code.
   *
   * @param source code as a String.
   */
  case class Programmatic(source: String) extends Source

  /**
   * A single [[File]] that contains the source code that requires compilation.
   *
   * Note that the [[Parser]] was not designed to handle the case of this [[File]] not being available. It is assumed
   * that whatever process that created the file has already verified that the exists and is accessible for reading.
   *
   * That being said, some errors do need to be handled by the [[Parser]], for example: if while reading the contents
   * of the [[File]], an unexpected EOF is encountered.
   *
   * @param file containing halla source.
   */
  case class FromFile(file: File) extends Source
}


