package org.halla.compiler.parse.ast.func

import org.halla.compiler.parse.ast.expr.Expression
import org.halla.compiler.parse.ast.expr.Expression.Ref
import org.halla.compiler.parse.ast.typedef.{GenericArgument, TypeRef}
import org.halla.compiler.parse.parsed.NS.NameSpace
import org.halla.compiler.parse.parsed.{NS, Parsed}
import parseback.Line

case class Function(namespace: NameSpace, name: String, genericArguments: List[GenericArgument], arguments: Map[Ref, TypeRef], returnType: TypeRef, values: Map[Ref, Expression], expression: Expression)(val loc: List[Line]) extends Parsed

object Function {

  import org.halla.compiler.parse._
  import parseback._

  type Value = (Ref, Expression)

  /**
   * Captures the definition of a function.
   */
  implicit lazy val function: Parser[NS[Function]] = {

    import Body._
    import Parameters._
    import Returns._
    import GenericParameters._

    implicit val W: Whitespace = Whitespace(spaceOrTab.*)

    ("def" ~> name ~ genericParameters ~ parameterList ~ returnType ~ bodyDefinition) ^^ {
      (loc: List[Line], name: String, genericParameters: List[GenericArgument], args: Map[Ref, TypeRef], returnType: TypeRef, namespacedBody: NS[Body]) =>
        NS(namespace => {
          val bodyDef = namespacedBody.unwrap(namespace)

          Function(namespace, name, genericParameters, args, returnType, bodyDef.values.toMap, bodyDef.expression)(loc)
        })
    }
  }

  private object GenericParameters {

    import Ref._
    import GenericArgument._

    implicit val W: Whitespace = Whitespace(spaceOrTab.*)

    lazy val genericParameters: Parser[List[GenericArgument]] = {
      ( "" ^^ { (_, _) => List.empty }
      | "[" ~> genericArguments <~ "]"
      )
    }
  }

  private object Parameters {

    import Ref._
    import TypeRef._

    implicit val W: Whitespace = Whitespace(spaceOrTab.*)

    lazy val parameter: Parser[(Ref, TypeRef)] = {
      ref ~ ":" ~ typeReference ^^ { (_, ref, _, typeRef) => ref -> typeRef }
    }
    lazy val parameters: Parser[Seq[(Ref, TypeRef)]] = {
      (parameters ~ ("," ~> parameter) ^^ { (_, paramList, param, paramType) => paramList :+ (param -> paramType) }
        | parameter ^^ { (loc, param, paramType) => Seq(param -> paramType) }
        )
    }

    lazy val parameterList: Parser[Map[Ref, TypeRef]] = {
      "(" ~> parameters <~ ")" ^^ { (_, parameters) => parameters.toMap }
    }
  }

  private object Returns {

    import TypeRef._

    implicit val W: Whitespace = spacesOrTabs

    lazy val returnType: Parser[TypeRef] =
      ":" ~> typeReference
  }

  case class Body(values: List[Value], expression: Expression)

  private object Body {

    import Expression._

    lazy val value: Parser[NS[Value]] = {
      implicit val W: Whitespace = spacesOrTabs

      import Ref._

      ("val" ~> ref <~ "=") ~ expression ^^ { (loc, valueRef, valueExpression) => {
        NS(namespace => {
          // append the new path part to the current namespace, wherever that is.
          val valueNamespace = namespace.append("$v$" + valueRef.symbol)

          valueRef -> valueExpression.unwrap(valueNamespace)
        })
      } }
    }

    lazy val values: Parser[List[NS[Value]]] = {
      implicit val W: Whitespace = Whitespace(anySpace)

      (value.? ^^ { (loc, maybeValue) => maybeValue.toList }
      | value ~ values ^^ { (loc, some, more) => more.+:(some) }
      )
    }

    lazy val bodyDefinition: Parser[NS[Body]] = {
      implicit val W: Whitespace = Whitespace(anySpace.*)

      import cats.implicits._

      "=" ~ "{" ~> values ~ expression <~ "}" ^^ { (loc, namespacedValues, expression) =>

        NS(namespace => {
          Body(namespacedValues.sequence.unwrap(namespace), expression.unwrap(namespace))
        })

      }
    }
  }

}

