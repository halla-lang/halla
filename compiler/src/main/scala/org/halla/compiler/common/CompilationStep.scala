package org.halla.compiler.common

abstract class CompilationStep[A, B](implicit configuration: CompilerConfiguration = CompilerConfiguration.default) {

  /**
   * Run the compilation step.
   *
   * @param input to the compilation step
   * @return Ongoing [[HResult]] to be used in later compilation steps.
   */
  def run(input: A): HResult[B]

}
