package org.halla.compiler.common

trait CompilerConfiguration {

  /**
   * Limit for concurrent processes
   * @return maximum number of threads that can run in parallel
   */
  def parallelism: Int
}

object CompilerConfiguration {
  lazy val default: CompilerConfiguration = new CompilerConfiguration {
    /**
     * Limit for concurrent processes
     *
     * @return maximum number of threads that can run in parallel
     */
    override def parallelism: Int = 1000
  }
}
