package org.halla.compiler.common.graph

import scala.languageFeature.higherKinds

trait Vertex[T] {

  /**
   * Calculate a unique ID for the vertex. This will be used to index it in the [[HGraph]].
   *
   * @param t to create unique id for
   * @return
   */
  def index(t: T): Symbol

}
