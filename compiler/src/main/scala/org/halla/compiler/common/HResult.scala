package org.halla.compiler.common

import cats.Applicative
import cats.data.NonEmptyChain
import org.halla.compiler.common.HResult.Success

/**
 * ADT representing the result of a compilation task. This is useful for error handling, especially during recursive
 * operations like type-resolving.
 *
 * @tparam T type of the inner result
 */
sealed trait HResult[+T] {

  def map[B](f: T => B): HResult[B] = {
    this match {

      case success@HResult.Success(value) =>
        success.copy(value = f.apply(value))

      case failures: HResult.Failure =>
        failures

    }
  }

  def flatMap[B](f: T => HResult[B]): HResult[B] = {
    this.map(f) match {
      case Success(value) => value
      case f: HResult.Failure => f
    }
  }

}

object HResult {

  /**
   * Category Definitions
   */

  case class Success[+T](value: T) extends HResult[T]

  case class Failure(errors: NonEmptyChain[CompilationError]) extends HResult[Nothing]

  object Failure {
    def apply(error: CompilationError, more: CompilationError*): Failure = {
      new Failure(NonEmptyChain(error, more: _*))
    }
  }

  /**
   * Type-class Definitions
   */

  implicit def applicative: Applicative[HResult] = new Applicative[HResult] {
    override def pure[A](x: A): HResult[A] = Success(x)
    override def ap[A, B](ff: HResult[A => B])(fa: HResult[A]): HResult[B] = fa.flatMap(a => ff.map(_(a)))
  }
}