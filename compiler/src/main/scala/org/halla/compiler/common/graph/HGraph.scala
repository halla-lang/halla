package org.halla.compiler.common.graph

trait HGraph[V, E] {

  /**
   * Find vertex by ID.
   *
   * @param vertexId to find
   * @return
   */
  def findVertex(vertexId: Symbol): Option[V]

  /**
   * Return vertices that are connected the vertex with the provided ID.
   *
   * @param vertexId to find connections for
   * @return
   */
  def findConnections(vertexId: Symbol): List[V]
  
}
