package org.halla.compiler.common

import org.halla.compiler.parse.parsed.Parsed
import org.halla.compiler.parse.parsed.Parsed.SrcMapped

/**
 * An error found within the source that the Compiler was tasked to compile.
 */
trait CompilationError extends SrcMapped {

  /**
   * @return explanation of the error.
   */
  def explanation: String

  /**
   * Suggestions for how to fix the error.
   *
   * @return
   */
  def suggestions: String = "If you think this Error is a problem with the Halla Compiler itself, please open an issue on https://gitlab.com/halla-lang/halla with steps for reproducing the problem."

  /**
   * Print the error to the console.
   */
  def print(): Unit = {
    println("Error found in source:")
    loc.foreach(line => {
      line.renderError
    })
    println(explanation)
    println()
  }
}

object CompilationError {
  case class InternalError(message: String) extends CompilationError {
    /**
     * @return explanation of the error.
     */
    override def explanation: String = message ++ " (This was more likely an issue with Halla's compiler, and not your source code. Please help out the community by creating an issue on Halla's repository!)"
    override def src: Seq[Parsed] = Seq.empty
  }
}
