package org.halla.compiler.lambdify.lambda.patternmatching

object BranchLambdifier {

//  def lambdifyBranch(branchCase: Expression.Match.MatchCase, scope: TraversingScope) =
//      (branchCase match {
//        // create scope from case constructor match
//        case original@Match.CaseConstructorMatch(target, arguments: List[Ref]) =>
//          for {
//            // try to figure out which type the match refers to
//            targetType <- scope.seekTypeRef(target)
//
//            // check if the target type is valid or not
//            targetTypeArgumentTypes <- targetType match {
//              case Composite(typeRef, _) =>
//                Success(typeRef.asRef.typeArguments)
//              case tu: TypeUnion =>
//                ???
//            }
//
//            // lambdify each the arguments from the case constructor match
//            lambdifiedArguments <- arguments.mapWithIndex({ case (argument: Ref, ndx: Int) =>
//              // find the type of each case argument
//              scope.seekTypeRef(targetTypeArgumentTypes(ndx)).map(t => {
//                CaseArgument(argument.symbol, t)(argument)
//              })
//            }).sequence
//
//            // use all the case args to build a scope for the case body
//            branchScope = Scope(lambdifiedArguments.map(Value(_)), Map.empty, scope)
//
//            // use scope to lambdify the body of the branch
//            bodyLambda <- Lambdifier.lambdifyExpression(branchScope, branchCase.expression)
//          } yield {
//            PatternMatch.CaseConstructorBranch(targetType = targetType, args = lambdifiedArguments, body = bodyLambda)(original)
//          }
//      })
}
