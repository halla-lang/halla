package org.halla.compiler.lambdify.lambda.natives
import cats.data.Chain
import org.halla.compiler.lambdify.Lambda.Definition
import org.halla.compiler.lambdify.Lambda.Definition.{FunctionReference, NativeFunctionReference}
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.lambdify.lambda.Type.ConcreteType

trait NativeFunction {
  def symbol: String
  def t: Type
  def asFunctionReference: Definition = {
    NativeFunctionReference(symbol, this)
  }
}
