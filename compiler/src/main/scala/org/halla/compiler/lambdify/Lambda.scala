package org.halla.compiler.lambdify

import cats.data.{Chain, NonEmptyList}
import org.halla.compiler.lambdify.Lambda.Definition.CaseArgument
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.lambdify.lambda.natives.NativeFunction
import org.halla.compiler.parse.ast.expr.Expression
import org.halla.compiler.parse.parsed.Parsed
import org.halla.compiler.parse.parsed.Parsed.SrcMapped

/**
 * One of the nodes in a [[Lambda]] tree, which essentially is a AST expressed with
 * a format much akin to Lambda Calculus.
 *
 * It is used to express what the scope (context) for each Lambda in a given program's
 * Lambda calculus contains. This allows for references to be traced and for type-inference.
 *
 * Problem to deal with in the future (TODO):
 * If a term in a child scope contains a ref shares the same name as a ref in a
 * higher-level scope, it should evict and replace the parent item in the scope, effectively
 * obscuring it from any scopes below.
 * Maybe it should just throw an error instead? How does this affect implicits?
 */
sealed trait Lambda extends SrcMapped {
  def t: Type
}

object Lambda {

  type Definitions = List[Definition]

  /**
   * An explicitly defined [[Lambda]] that can be exposed to a scope and then accessed by references within that scope.
   *
   * Examples are:
   * - Function arguments
   * - Value definitions (ex: `val swag = 420`)
   */
  sealed trait Definition extends SrcMapped {
    def symbol: String

    def t: Type
  }

  object Definition {

    /**
     * A value to memoize, created via the "val" keyword for use a function or module.
     */
    case class Memo(symbol: String, value: Lambda)(val src: Seq[Parsed]) extends Definition {
      override def t: Type = value.t
    }

    /**
     * Argument for a function, which can be accessed by members of its body.
     */
    case class FunctionArgument(symbol: String, index: Int, t: Type)(val src: Seq[Parsed]) extends Definition


    /**
     * An argument to a used in a case constructor pattern match
     */
    case class CaseArgument(symbol: String, index: Int, t: Type)(val src: Seq[Parsed]) extends Definition

    /**
     * Reference to a function.
     *
     * Q: Why not just use the [[Function]] directly?
     * A: It is important that functions can still be referenced even when they have yet to be fully lambdified in order
     * to properly handle recursion and references between functions defined in the same scope.
     */
    case class FunctionReference(symbol: String, args: Chain[FunctionArgument], assumedType: Type, returnType: Type)(val src: Seq[Parsed]) extends Definition {
      override def t: Type = assumedType
    }

    case class NativeFunctionReference(symbol: String, n: NativeFunction) extends Definition {
      override def t: Type = n.t
      override def src: Seq[Parsed] = Nil
    }

  }


  /**
   * A literal instance of a [[org.halla.compiler.lambdify.lambda.Type.Native]] type.
   *
   * Prime examples are declarations of strings or numbers such as:
   * - `"yolo"`
   * - `1337`
   *
   * @param value of the literal
   * @param t     representing the literal's type
   * @param src   for source mapping
   */
  case class Literal(value: Any, t: Type.Native)(val src: Seq[Parsed]) extends Lambda

  /**
   * A [[Lambda]] that references a definition
   */
  case class Reference(referred: Definition) extends Lambda {
    override def t: Type = referred.t

    override def src: Seq[Parsed] = referred
  }

  /**
   * Application of one [[Lambda]] to another [[Lambda]] as an argument.
   *
   * @param left  lambda that should accept the right as a parameter
   * @param right parameter that will be applied
   */
  case class Application(left: Lambda, right: Lambda) extends Lambda {
    override def t: Type = Type.Lambda(left.t, right.t)

    override def src: Seq[Parsed] = left.src ++ right.src
  }


  /**
   * Pattern Match statement.
   *
   * @param subject  to match
   * @param branches for each case
   */
  case class PatternMatch(subject: Lambda, branches: NonEmptyList[PatternMatch.Branch]) extends Lambda {
    override lazy val t: Type = branches.map(_.body.t).reduce

    override def src: Seq[Parsed] = subject.src
  }

  object PatternMatch {

    sealed trait Branch {
      def body: Lambda
    }

    case class CaseConstructorBranch(targetType: Type.Composite, args: List[CaseArgument], body: Lambda)(val src: Expression.Match.CaseConstructorMatch) extends Branch

  }


}