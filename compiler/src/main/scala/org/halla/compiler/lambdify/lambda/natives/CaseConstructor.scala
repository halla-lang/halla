package org.halla.compiler.lambdify.lambda.natives

import org.halla.compiler.lambdify.lambda.Type

case class CaseConstructor(symbol: String, constructorFor: Type.Composite) extends NativeFunction {
  override def t: Type = constructorFor.args.foldRight[Type](constructorFor)((arg, prev) => arg.t -> prev)
}
