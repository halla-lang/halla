package org.halla.compiler.lambdify

import cats.data.{Chain, NonEmptyList}
import org.halla.compiler.common.HResult.{Failure, Success}
import org.halla.compiler.common.{CompilationError, CompilationStep, HResult}
import org.halla.compiler.lambdify.Lambda.Definition.{CaseArgument, FunctionArgument, FunctionReference, Memo, NativeFunctionReference}
import org.halla.compiler.lambdify.Lambda.{Definition, Literal, PatternMatch, Reference}
import org.halla.compiler.lambdify.Lambdified.DefinedFunction
import org.halla.compiler.lambdify.failures.LambdifierError.{EmptyTypeUnion, UnresolvedReference}
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.lambdify.lambda.Type.{Composite, TypeUnion, Variable}
import org.halla.compiler.lambdify.lambda.composite.Member
import org.halla.compiler.lambdify.lambda.natives.CaseConstructor
import org.halla.compiler.lambdify.referential.TraversingScope.Scope
import org.halla.compiler.parse.ast.Package
import org.halla.compiler.parse.ast.expr.Expression
import org.halla.compiler.parse.ast.expr.Expression.{Match, Ref}
import org.halla.compiler.parse.ast.func.Function
import org.halla.compiler.parse.ast.typedef.TypeDefinition

object Lambdifier extends CompilationStep[Package, Lambdified] {

  import cats.implicits._

  /**
   * Run the compilation step.
   *
   * @param input to the compilation step
   * @return Ongoing [[HResult]] to be used in later compilation steps.
   */
  override def run(input: Package): HResult[Lambdified] = {

    val initialPackageScope = Scope(input.namespace)

    for {

      // collect all types in the package so they can be referenced
      scopeWithTypes <- collectTypes(initialPackageScope, input)

      // lambdify all functions in the package
      functions <- lambdifyFunctions(scopeWithTypes, input.functionDefinitions)

    } yield {
      // extract lambdified types from the scope
      val lambdifiedTypes = scopeWithTypes.types
      Lambdified.Package(functions, lambdifiedTypes, None)
    }
  }

  private def collectTypes(packageScope: Scope, input: Package): HResult[Scope] = {

    def typesFromDefinition(td: TypeDefinition): HResult[List[Type]] = {
      td match {
        case TypeDefinition.ADT(typeRef, genericArguments, variations, nameSpace) =>
          // add generic arguments to scope for local type resolution
          val scopeWithGenerics = packageScope.add(types = {
            // create type for each generic argument
            genericArguments.map(genericArgument => {
              Type.Variable(genericArgument.name)
            })
          })

          for {

            // try to define types each of the ADT's variations
            typedVariations <- variations.toList.map(variation => {
              for {
                members <- variation.typeArguments.map(typeArgRef => {
                  scopeWithGenerics.seekTypeRef(typeArgRef).map(Member(_))
                }).sequence
              } yield {
                // create case definition from ast
                val variationAsDef = TypeDefinition.CaseDefinition(variation.name, variation.typeArguments, nameSpace)(variation.loc)

                // link the variation's typeRef to a new Lambdified Type.
                Type.Composite(variationAsDef, members)
              }
            }).sequence

            // insure the variations are nonEmpty
            validVariations <- NonEmptyList.fromList(typedVariations) match {
              case Some(nonEmptyVariations) =>
                Success(nonEmptyVariations)
              case None =>
                Failure(EmptyTypeUnion(td))
            }
          } yield {
            // construct the ADT using the typed variations
            Type.TypeUnion.Declared(td, validVariations) +: typedVariations
          }

        case TypeDefinition.CaseDefinition(symbol, typeArguments, nameSpace) =>
          // the Parser is currently incapable of producing case definitions, so this case should never happen
          Failure(CompilationError.InternalError("Parser produced a CaseDefinition, which is not yet supported."))

        // all native types are already defined in native scope
        case TypeDefinition.IntDefinition =>
          Success(List.empty)
        case TypeDefinition.FloatDefinition =>
          Success(List.empty)
        case TypeDefinition.StringDefinition =>
          Success(List.empty)
      }
    }

    /**
     * Used to inject automatically generated functions (like constructors) based on types into a scope.
     * @param t
     * @return
     */
    def functionsFromTypes(t: Type): List[Definition] = {
      t match {
        // generate constructors for composite types
        case composite@Composite(typeDef, args) =>
          List(CaseConstructor(typeDef.symbol, composite).asFunctionReference)
        case _ =>
          List.empty
      }
    }

    for {
      // iterate through each of the package's types
      sequencedTypesFromPackage <- input.types.toList.map(td => {
        // and attempt to create multiple lambdified types from EACH of them (one -> many)
        typesFromDefinition(td)
      }).sequence

      // combine all the types into a single list
      typesFromPackage = sequencedTypesFromPackage.flatten

      // generate functions like constructors from the types so that they can be referenced later
      functions = typesFromPackage.flatMap(functionsFromTypes)
    } yield {
      // use those types to define a scope
      packageScope.add(
        definitions = functions,
        types = typesFromPackage
      )
    }
  }


  private def lambdifyFunctions(scope: Scope, functions: Chain[Function]): HResult[Chain[DefinedFunction]] = {

    import cats.implicits._

    // iterate through the functions
    functions.map {

      // convert each function to a Referable
      case fd@Function(nameSpace, name, genericArguments, arguments, returnTypeRef, _, _) =>
        import Type._

        // add generic arguments to function scope for local type resolution
        val scopeWithGenerics = scope.add(types = {
          // create type for each generic argument
          genericArguments.map(genericArgument => {
            Type.Variable(genericArgument.name)
          })
        })

        for {
          // find the type of each argument
          args <- Chain.fromSeq(arguments.toSeq.zipWithIndex.map({ case ((ref, typeRef), index: Int) =>
            scopeWithGenerics.seekTypeRef(typeRef).map(returnType => {
              FunctionArgument(ref.symbol, index, returnType)(ref)
            })
          })).sequence

          // find the type definition for the function's return type
          returnType <- scopeWithGenerics.seekTypeRef(returnTypeRef)
        } yield {
          // create a reference for the function that can be used by any lambdas with access
          val functionTypeSignature = args.map(_.t).foldRight[Type](returnType)({ case (l, r) => l -> r })
          FunctionReference(name, args, functionTypeSignature, returnType)(fd)
        }
    }.sequence.flatMap(functionReferences => {

      // combine the functions into a final scope that contains all the referable definitions
      val packageScopeWithFunctions = scope.add(definitions = functionReferences.toList)

      // use this new package level Scope to lambdify each of the functions originally provided to the function
      functions.map(lambdifyFunction(packageScopeWithFunctions, _)).sequence
    })
  }

  def lambdifyFunction(currentScope: Scope, function: Function): HResult[DefinedFunction] = {
    function match {
      // function defined by the user
      case Function(nameSpace, name, genericArguments, arguments, returnTypeRef, values, body) =>
        // get all the information about the function that we derived when the scope was initially scanned, and also
        // insure that the function can reference itself.
        currentScope.seekRef(name).map {
          case Memo(symbol, value) =>
            // looking for a function but found something else
            ???

          case FunctionArgument(symbol, index, t) =>
            // looking for a function but found something else
            ???

          case CaseArgument(symbol, index, t) =>
            // looking for a function but found something else
            ???

          case NativeFunctionReference(symbol, n) =>
            // looking for a function but found something else
            ???

          case FunctionReference(symbol, args, typeSignature, returnType) =>
            // use referable function arguments to create function's inner scope
            val baseFunctionScope = Scope(function.namespace, currentScope)
              // add the args to the inner function scope
              .add(args.toList)

            // add generic arguments to function scope for local type resolution
            val scopeWithGenerics = baseFunctionScope.add(types = {
              // create type for each generic argument
              genericArguments.map(genericArgument => {
                Type.Variable(genericArgument.name)
              })
            })

            // add val definitions to function scope
            values.foldLeft[HResult[Scope]](Success(scopeWithGenerics))({ case (scope, valueDefinition) => {
              val (valueRef, valueExpression) = valueDefinition

              for {
                currentScope <- scope

                // TODO: lazily compose this expression so that it has access to itself and other vals
                lambdifiedExpression <- lambdifyExpression(currentScope, valueExpression)
              } yield {
                val memo = Memo(valueRef.symbol, lambdifiedExpression)(valueRef ++ valueExpression)

                currentScope.add(definitions = List(memo))
              }
            }
            }).flatMap(innerFunctionScope => {
              // use the function's inner scope to lambdify its body
              lambdifyExpression(innerFunctionScope, body).map(lambdifiedBody => {
                // use all of the above to produce a defined function
                DefinedFunction(name, args.toList, lambdifiedBody, typeSignature)(function)
              })
            })
        }.getOrElse({
          // the function could not be found in the scope, meaning it must have never been made referable.
          // This is likely an error in this code's logic, rather than an issue with the source being compiled.
          ???
        })

    }
  }

  def lambdifyExpression(scope: Scope, expression: Expression): HResult[Lambda] = {
    expression match {
      case expression: Expression.SimpleExpression =>
        expression match {
          case Expression.Access(accessed, property) =>
            ???

          case Expression.Application(left, right) =>
            for {
              // lambdify the left expression...
              lambdaL <- lambdifyExpression(scope, left)
              // ...and right expression of the application
              lambdaR <- lambdifyExpression(scope, right)
            } yield {
              // use both sides to create the lambda
              Lambda.Application(lambdaL, lambdaR)
            }

          case Expression.FloatLiteral(value) =>
            Success(Literal(value, Type.Native.F)(expression))

          case Expression.IntegerLiteral(value) =>
            Success(Literal(value, Type.Native.I)(expression))

          case Expression.StringLiteral(value) =>
            Success(Literal(value, Type.Native.S)(expression))

          case r@Expression.Ref(symbol) =>
            scope.seekRef(symbol) match {

              case Some(foundRef: Definition) =>
                Success(Reference(foundRef))

              case None =>
                Failure(UnresolvedReference(r))
            }
        }

      case expression: Expression.ControlExpression =>
        expression match {
          case Expression.Match(subject, branches) =>
            val lambdifiedSubject = lambdifyExpression(scope, subject)

            // lambdify each of the branches
            val lambdifiedBranches: HResult[NonEmptyList[PatternMatch.Branch]] = branches.map({ case (branchCase: Match.MatchCase) =>
              // create scope based on the type of pattern match used
              (branchCase match {
                // create scope from case constructor match
                case original@Match.CaseConstructorMatch(target, arguments) =>
                  for {
                    // try to figure out which type the match refers to
                    targetType <- scope.seekTypeRef(target)

                    // check if the target type is valid or not
                    validTargetType <- targetType match {
                      case composite: Composite =>
                        Success(composite)
                      case concrete: Type.ConcreteType =>
                        ???
                      case tu: TypeUnion =>
                        ???
                      case lambda: Type.Lambda =>
                        ???
                      case variable: Variable =>
                        ???
                    }

                    // get the target type's parameters
                    targetTypeArgumentTypes = validTargetType.args.map(_.t)

                    // lambdify each the arguments from the case constructor match
                    lambdifiedArguments = arguments.mapWithIndex({ case (argument: Ref, ndx: Int) =>
                      // find the type of each case argument
                      val t = targetTypeArgumentTypes(ndx)
                      CaseArgument(argument.symbol, ndx, t)(argument)
                    })

                    // build a scope for the case body
                    branchScope = Scope(branchCase.namespace, scope)
                      // add case args to the branch scope
                      .add(lambdifiedArguments)

                    // use scope to lambdify the body of the branch
                    bodyLambda <- lambdifyExpression(branchScope, branchCase.expression)
                  } yield {
                    PatternMatch.CaseConstructorBranch(targetType = validTargetType, args = lambdifiedArguments, body = bodyLambda)(original)
                  }
              })
            }).sequence

            lambdifiedSubject.flatMap(sub => {
              lambdifiedBranches.map(brs => {
                Lambda.PatternMatch(sub, brs)
              })
            })
        }
    }
  }
}
