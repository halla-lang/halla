package org.halla.compiler.lambdify.lambda

import cats.Semigroup
import cats.data.{NonEmptyChain, NonEmptyList}
import org.halla.compiler.lambdify.lambda.composite.Member
import org.halla.compiler.parse.ast.typedef.TypeDefinition
import org.halla.compiler.parse.ast.typedef.TypeDefinition.{FloatDefinition, IntDefinition, StringDefinition}
import org.halla.compiler.parse.parsed.Parsed
import org.halla.compiler.parse.parsed.Parsed.SrcMapped

import scala.language.implicitConversions

sealed trait Type

object Type {

  /**
   * Type of a simple Lambda that accepts one parameter and returns a result. This type can be chained together to
   * produce more complex functional types such as a function with multiple parameters.
   *
   * @param input parameter
   * @param output result
   */
  case class Lambda(input: Type, output: Type) extends Type

  /**
   * A composite type, which are Halla's alternative to scala's case classes.
   *
   * @param typeDef for the defined type
   */
  case class Composite(typeDef: TypeDefinition, args: List[Member]) extends Type with ConcreteType {
    override def symbol: String = typeDef.symbol
    override def src: Seq[Parsed] = typeDef
  }

  /**
   * A variable type, for generic typing.
   * @param symbol unique symbol for the variable type
   */
  case class Variable(symbol: String) extends Type

  /**
   * A native type.
   */
  sealed trait Native extends Type with ConcreteType {
    def parsed: TypeDefinition
    override def symbol: String = parsed.symbol
    override def src: Seq[Parsed] = parsed
  }

  object Native {

    /**
     * Halla's native integer type.
     */
    object I extends Native {
      override def parsed: TypeDefinition = IntDefinition
    }

    /**
     * Halla's native string type.
     */
    object S extends Native {
      override def parsed: TypeDefinition = StringDefinition
    }

    /**
     * Halla's native float type.
     */
    object F extends Native {
      override def parsed: TypeDefinition = FloatDefinition
    }

    /**
     * Exit is the return type for an executable halla program.
     */
    object ExitCode extends Native {
      override def parsed: TypeDefinition = FloatDefinition
    }
  }

  /**
   * A Concrete Type is a non-generic one. The distinction is important, as the goal of the resolver will be to
   * remove as many instances of non-Concrete Types as possible from the lambdified AST.
   *
   * See documentation on Resolver for more info.
   */
  sealed trait ConcreteType extends SrcMapped with Type { this: Type => def symbol: String }

  /**
   * A collection of multiple Types.
   *
   */
  sealed trait TypeUnion extends Type {
    def types: NonEmptyList[Type]
  }
  object TypeUnion {
    case class Declared(typeDef: TypeDefinition, types: NonEmptyList[Type]) extends TypeUnion with ConcreteType {
      override def symbol: String = typeDef.symbol
      override def src: Seq[Parsed] = typeDef
    }
    case class Anonymous(types: NonEmptyList[Type]) extends TypeUnion
    def apply(types: NonEmptyList[Type]): Anonymous = Anonymous(types)
  }

  /**
   * Create a [[Type]] from a collection of one or more instances of [[Type]].
   *
   * Input to this function is flattened, making it very useful for insuring that no instances of [[TypeUnion]] become
   * nested within one another.
   *
   * It might be worthwhile to force a distinction between [[TypeUnion]] and the other variants of [[Type]].
   *
   * This function is the backbone of Type's Semigroup definition
   *
   * @param types to create a single [[Type]] from
   * @return
   */
  def fromChain(types: NonEmptyChain[Type]): Type = {

    //TODO: Make sure that no two types:
    // 1) share the same symbol and path
    //         ***  AND  ***
    // 2) be different in any way
    // this would mean the types were likely defined by mistake and also renders them both unusable in the package
    // they were originally defined in.

    /**
     * Function used to recursively flatten Types. This removes NestedTypeUnions.
     * @param t is the type that will be flattened
     * @return
     */
    def flatten(t: Type): NonEmptyChain[Type] = {
      t match {
        case Lambda(_, _) => NonEmptyChain(t)
        case Composite(_, _) => NonEmptyChain(t)
        case Variable(_) => NonEmptyChain(t)
        case n: Native => NonEmptyChain(n)
        case tu: TypeUnion =>
          NonEmptyChain.fromNonEmptyList(tu.types).flatMap(flatten _)
      }
    }

    // flatten the types in case there are any instances of nested [[TypeUnion]]
    val flattenedTypes: NonEmptyList[Type] = types.flatMap(flatten).toNonEmptyList

    // check the result
    flattenedTypes match {
      case NonEmptyList(head, tail) =>
        tail match {
          case Nil =>
            // flattenedTypes was a single type
            head
          case ::(head, tl) =>
            // flattenedTypes was a collection of more than 1 type
            TypeUnion(flattenedTypes)
        }
    }
  }

  implicit def arrowSyntax(tupled: (Type, Type)): Type = {
    Lambda(tupled._1, tupled._2)
  }

  implicit def arrowSyntax2(tupled: ((Type, Type), Type)): Type = {
    Lambda(tupled._2, tupled._1)
  }

  implicit val typeSemigroup: Semigroup[Type] = new Semigroup[Type] {
    override def combine(x: Type, y: Type): Type = {
      if (x.equals(y)) {
        x
      } else {
        // resolve type unions
        ???
//        fromChain(NonEmptyChain(x, y))
      }
    }
  }
}
