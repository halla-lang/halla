package org.halla.compiler.lambdify.referential

import org.halla.compiler.common.HResult
import org.halla.compiler.common.HResult.Failure
import org.halla.compiler.lambdify.Lambda.Definition
import org.halla.compiler.lambdify.failures.LambdifierError.UnresolvedTypeReference
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.lambdify.lambda.natives.Math.IntegerAddition
import org.halla.compiler.parse.ast.typedef.TypeDefinition.{FloatDefinition, IntDefinition, StringDefinition}
import org.halla.compiler.parse.ast.typedef.TypeRef
import org.halla.compiler.parse.parsed.NS.NameSpace
import org.halla.compiler.parse.parsed.NS.NameSpace.NativeSpace

sealed trait TraversingScope {

  def namespace: NameSpace

  def definitions: List[Definition]

  def types: List[Type]

  def seekRef(symbol: String): Option[Definition]

  def seekTypeRef(typeRef: TypeRef): HResult[Type]
}

object TraversingScope {

  /**
   * The top level scope
   */
  case object NativeScope extends TraversingScope {

    override def namespace: NameSpace = NativeSpace

    lazy val definitions: List[Definition] = List(
      IntegerAddition
    ).map(_.asFunctionReference)

    override def seekRef(symbol: String): Option[Definition] = {
      definitions.find(func => func.symbol.equals(symbol))
    }

    lazy val indexedTypes: Map[String, Type] = Map(
      IntDefinition.symbol -> Type.Native.I,
      FloatDefinition.symbol -> Type.Native.F,
      StringDefinition.symbol -> Type.Native.S
    )

    override def types: List[Type] = List(Type.Native.I, Type.Native.F, Type.Native.S)

    override def seekTypeRef(typeRef: TypeRef): HResult[Type] = indexedTypes.get(typeRef.name).map(HResult.Success(_)).getOrElse(Failure(UnresolvedTypeReference(typeRef)))
  }

  case class Scope(definitions: List[Definition], types: List[Type], parent: TraversingScope, namespace: NameSpace) extends TraversingScope {

    override def seekRef(symbol: String): Option[Definition] = {
      definitions.find(func => func.symbol.equals(symbol)) match {
        case Some(value) =>
          Some(value)
        case None =>
          parent.seekRef(symbol)
      }
    }

    lazy val indexedTypes: Map[String, Type] = {
      types.map {
        case g@Type.Variable(name) =>
          name -> g
        case concreteType: Type.ConcreteType =>
          concreteType.symbol -> concreteType
        case _ => ???
      }.toMap
    }

    override def seekTypeRef(typeRef: TypeRef): HResult[Type] = {
      indexedTypes.get(typeRef.name).map(HResult.Success(_)).getOrElse(parent.seekTypeRef(typeRef))
    }

    def add(definitions: List[Definition] = List.empty, types: List[Type] = List.empty): Scope = {
      this.copy(
        definitions = (this.definitions ++ definitions).distinct,
        types = this.types ++ types
      )
    }
  }

  object Scope {

    def apply(nameSpace: NameSpace, parentScope: Scope): Scope = {
      new Scope(List.empty, List.empty, parentScope, nameSpace)
    }

    def apply(nameSpace: NameSpace): Scope = {
      new Scope(List.empty, List.empty, NativeScope, nameSpace)
    }
  }

}
