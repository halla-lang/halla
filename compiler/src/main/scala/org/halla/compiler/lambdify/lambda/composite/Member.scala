package org.halla.compiler.lambdify.lambda.composite

import org.halla.compiler.lambdify.lambda.Type

/**
 * A member of a Composite Type.
 */
sealed trait Member {
  def t: Type
}

object Member {

  case class Anonymous(t: Type) extends Member

  case class Named(symbol: String, t: Type) extends Member

  def apply(t: Type): Member = {
    Anonymous(t)
  }
}
