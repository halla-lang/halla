package org.halla.compiler.lambdify.failures

import org.halla.compiler.common.CompilationError
import org.halla.compiler.parse.ast.expr.Expression.Ref
import org.halla.compiler.parse.ast.typedef.{TypeDefinition, TypeRef}
import org.halla.compiler.parse.parsed.Parsed

import scala.language.implicitConversions

sealed trait LambdifierError extends CompilationError

object LambdifierError {

  case object IncompatibleInput extends LambdifierError {
    /**
     * @return explanation of the error
     */
    override def explanation: String = "Incompatible input provided to Lambdifier."

    override def src: Seq[Parsed] = Seq.empty
  }

  case class UnresolvedReference(ref: Ref) extends LambdifierError {
    /**
     * @return explanation of the error.
     */
    override def explanation: String = s"Could not resolve: ${ref.symbol}. Where did it come from?"

    override def src: Seq[Parsed] = ref
  }

  case class UnresolvedTypeReference(typeRef: TypeRef) extends LambdifierError {
    /**
     * @return explanation of the error.
     */
    override def explanation: String = s"Could not resolve the type: ${typeRef.name}. Where did it come from?"

    override def src: Seq[Parsed] = typeRef
  }

  case class EmptyTypeUnion(union: TypeDefinition) extends LambdifierError {
    /**
     * @return explanation of the error.
     */
    override def explanation: String = s"${union.symbol} needs at least 1 unique variation, otherwise you wouldn't be able to construct it!"

    override def src: Seq[Parsed] = union
  }
}
