package org.halla.compiler.lambdify

import cats.data.Chain
import org.halla.compiler.lambdify.Lambda.Definition.FunctionArgument
import org.halla.compiler.lambdify.failures.LambdifierError
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.parse.parsed.Parsed
import org.halla.compiler.parse.parsed.Parsed.SrcMapped

sealed trait Lambdified extends SrcMapped {
  def functions: Chain[Lambdified.Function]
  def types: List[Type]
}

object Lambdified {

  sealed trait Function extends SrcMapped {
    def symbol: String
    def t: Type
  }

  /**
   * A function that was parsed and lambdified from halla source code.
   */
  case class DefinedFunction(symbol: String, arguments: List[FunctionArgument], body: Lambda, t: Type)(val src: Seq[Parsed]) extends Function


  /**
   * A [[Package]] represents the top level scope of an entire package.
   */
  case class Package(functions: Chain[Function], types: List[Type], err: Option[LambdifierError]) extends Lambdified {
    override def src: Seq[Parsed] = functions.toList.flatMap(_.src)
  }

}
