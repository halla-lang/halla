package org.halla.compiler.lambdify.lambda.natives

import org.halla.compiler.lambdify.lambda.Type

object Math {
  import org.halla.compiler.lambdify.lambda.Type.Native._

  case object IntegerAddition extends NativeFunction {
    override def t: Type = I -> I -> I
    override def symbol: String = "+"
  }
}
