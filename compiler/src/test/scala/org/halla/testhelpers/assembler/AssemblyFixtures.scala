package org.halla.testhelpers.assembler

import java.lang.reflect.Method

import org.halla.compiler.assemble.asm.ASMResult
import org.halla.compiler.assemble.asm.ASMResult.Success
import org.scalatest.Assertions

trait AssemblyFixtures { this: Assertions =>

  /**
    * inspect some bytecode, generating a class from it
    *
    * @param namespace namespace
    * @param className class name
    * @param byteCode assembled code
    * @return
    */
  def inspect(namespace: Seq[String], className: String, byteCode: Array[Byte])(implicit loader: DynamicClassLoader): Class[_] = {
    // check that the code compiles to a class:
    loader.define(s"halla.${namespace.mkString(".")}.$className", byteCode)
  }


  /**
    * evaluate the result of invoking a method on a built package.
    *
    * @param assembledResult assembler result
    * @param testTarget method to invoke
    * @return
    */
  def eval(assembledResult: ASMResult, testTarget: String)(implicit loader: DynamicClassLoader): Any = {
    assembledResult match {
      case Success(namespace, name, code) =>
        eval(namespace, name, code, testTarget)
    }
  }

  /**
    * evaluate the result of some bytecode
    *
    * @param namespace namespace
    * @param className class name
    * @param byteCode assembled code
    * @param testTarget method to invoke
    * @return
    */
  def eval(namespace: Seq[String], className: String, byteCode: Array[Byte], testTarget: String, args: Object*)(implicit loader: DynamicClassLoader): Any = {
    // try to compile result to to a class:
    val clazz: Class[_] = inspect(namespace, className, byteCode)

    try {
      // run the function class that was just compiled:
      val method: Method = clazz.getMethod(testTarget, args.map(_.getClass): _*)

      // return the result of the method being invoked
      method.invoke(null, args: _*)
    } catch {
      case e: NoClassDefFoundError =>
        fail(s"Missing class: ${e.getMessage}", e)
      case npe: NullPointerException =>
        fail(s"NPE thrown. Likely caused by no static method with name `$testTarget` being found on class `$className`.", npe)
    }
  }

  /**
    * Inspect the result of the generated class
    *
    * @param assembledResult assembler result
    * @return
    */
  def inspect(assembledResult: ASMResult)(implicit loader: DynamicClassLoader): Class[_] = {
    assembledResult match {
      case Success(namespace, name, code) =>
        inspect(namespace, name, code)
    }
  }
}
