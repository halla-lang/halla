package org.halla.testhelpers.assembler

import com.typesafe.scalalogging.LazyLogging

/**
  * Class loader used to load the compiled class
 *
  * @param classLoader to load code with
  */
class DynamicClassLoader(classLoader: ClassLoader) extends ClassLoader(classLoader) with LazyLogging {
  def define(className: String, byteCode: Array[Byte]): java.lang.Class[_] = {
    super.defineClass(className, byteCode, 0, byteCode.length)
  }
}

object DynamicClassLoader {
  /**
    * create new loader from the current thread's. we will use this to try to run the assembled bytecode.
    * @return the loader
    */
  def createLoader(): DynamicClassLoader = {
    // get the current thread's classloader so we can inject classes into it
    val testClassLoader: ClassLoader = Thread.currentThread().getContextClassLoader

    new DynamicClassLoader(testClassLoader)
  }
}
