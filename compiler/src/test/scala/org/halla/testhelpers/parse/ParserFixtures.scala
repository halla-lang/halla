package org.halla.testhelpers.parse

import com.typesafe.scalalogging.LazyLogging
import org.halla.compiler.parse.parsed.NS
import org.scalatest.Assertion
import org.scalatest.freespec.AnyFreeSpec
import parseback.Parser
import parseback.util.Catenable
import parseback.util.Catenable.{Append, Empty, Single}

import scala.language.implicitConversions

trait ParserFixtures[F] extends LazyLogging {
  this: AnyFreeSpec =>

  implicit def deNamespaceParser(namespacedParser: Parser[NS[F]]): Parser[F] = {
    namespacedParser.map(_.unwrap(ParsedResults.testNamespace))
  }

  def parser: Parser[F]

  def parse(halla: String): F = {
    import cats.Eval
    import parseback._

    val input: LineStream[Eval] = LineStream[Eval](str = halla)

    @scala.annotation.tailrec
    def matchCat(cat: Catenable[F]): F = {
      cat match {
        case Single(actualResult) =>
          actualResult
        case Append(l, _) =>
          matchCat(l.apply())
        case Empty =>
          fail("Unexpected result came from AST construction")
      }
    }

    parser(input).value match {
      case Right(cat) =>
        matchCat(cat)
      case Left(errs) =>
        fail(s"Compilation failed. Caused by:\n" + errs.map({
          case loc: ParseError.WithLoc =>
            val sourceLine = loc.loc.base
            val errorArrowLine = s"${(0 until loc.loc.colNo).map({ _ => ' ' }).mkString}^"
            Seq(sourceLine, errorArrowLine).foreach((ln: String) => logger.error(ln))
            loc.render("test")
          case meta: ParseError.Meta =>
            meta.render("test2")
          case err@ParseError.UnexpectedEOF(expected) =>
            err.render("test3")
        }).mkString("\n"))
    }

  }

  def parse(halla: String, expectedResult: F): Unit = {
    val actualResult = parse(halla)
    assert(actualResult === expectedResult)
  }

  def hopelesslyParse(halla: String): Assertion = {
    import cats.Eval
    import parseback._

    val input: LineStream[Eval] = LineStream[Eval](str = halla)

    parser(input).value match {
      case Right(cat) =>
        cat match {
          case Single(actualResult) =>
            fail("The code was successfully parsed and did not throw an exception")
          case _ =>
            fail("The code was successfully parsed, but an unexpected result came from AST construction")
        }
      case Left(errs) =>
        succeed
    }
  }

}
