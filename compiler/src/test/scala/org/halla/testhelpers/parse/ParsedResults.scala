package org.halla.testhelpers.parse

import org.halla.compiler.parse.ast.Package
import org.halla.compiler.parse.ast.expr.Expression.Match.CaseConstructorMatch
import org.halla.compiler.parse.ast.expr.Expression.Ref
import org.halla.compiler.parse.ast.expr._
import org.halla.compiler.parse.ast.func.Function
import org.halla.compiler.parse.ast.imprt.Import
import org.halla.compiler.parse.ast.typedef.TypeDefinition.{ADT, CaseDefinition}
import org.halla.compiler.parse.ast.typedef._
import org.halla.compiler.parse.parsed.NS.NameSpace
import org.halla.compiler.parse.parsed.Parsed
import parseback.Line

object ParsedResults {
  import scala.language.implicitConversions
  implicit def convert[A](seq: Seq[A]): List[A] = seq.toList

  object TestParsed extends Parsed {
    override lazy val loc: List[Line] = testLines
  }

  lazy val testNamespace: NameSpace = NameSpace(path = Seq("org", "halla", "MyPackage"))

  lazy val testLines: List[Line] = List.empty

  // expression used in testing that is ignored by assertions
  lazy val emptyExpression: Expression = Ref("")(testLines)

  // Helpers for creating a Package

  lazy val emptyPackage: Package = Package(namespace = testNamespace, imports = Seq(), types = Seq(), functions = Seq())(testLines)

  def packag3(namespace: Seq[String], name: String, imports: Seq[Import], types: Seq[ADT] = Seq(), functions: Seq[Function] = Seq()): Package = {
    Package(namespace = NameSpace(namespace :+ name), imports = imports, types = types, functions = functions)(testLines)
  }

  // Helpers for creating an Expression

  import Expression._

  def application(left: SimpleExpression, right: SimpleExpression): Application = {
    Application(left = left, right = right)(loc = Seq())
  }

  def access(accessed: Ref, property: Ref): Access = {
    Access(accessed = accessed, property = property)(loc = Seq())
  }

  def ref(name: String): Ref = {
    Ref(name)(loc = Seq())
  }

  def stringLiteral(value: String): StringLiteral = {
    StringLiteral(value = value)(loc = Seq())
  }

  def integerLiteral(value: Int): IntegerLiteral = {
    IntegerLiteral(value = value)(loc = Seq())
  }

  def floatLiteral(value: Float): FloatLiteral = {
    FloatLiteral(value = value)(loc = Seq())
  }

  def operator(value: String): Ref = {
    ref(value)
  }

  def matchStatement(subject: Ref, cases: Seq[CaseConstructorMatch]): Match = {
    import cats.data._
    val matchCases = NonEmptyChain.fromSeq(cases).getOrElse({ throw new RuntimeException("Fixture for test was a Pattern Match without cases!")})

    Match(subject = subject, cases = matchCases.toNonEmptyList)(loc = Seq())
  }

  def matchCase(target: TypeRef, arguments: Seq[Ref] = Seq(), expression: Expression): CaseConstructorMatch = {
    CaseConstructorMatch(target = target, arguments = arguments)(expression = expression, namespace = testNamespace, loc = Seq())
  }

  // Helpers for creating type definitions

  def adt(baseRef: TypeRef, variations: Seq[TypeRef] = Seq.empty): ADT = {
    ADT(symbol = baseRef.name, genericArguments = Seq(), variations = variations, testNamespace)(loc = Seq())
  }

  def adt(symbol: String, genericArguments: Seq[GenericArgument], variations: Seq[TypeRef]): ADT = {
    ADT(symbol = symbol, genericArguments = genericArguments, variations = variations, testNamespace)(loc = Seq())
  }

  def genericAdt(baseRef: TypeRef, genericArguments: Seq[GenericArgument] = Seq.empty, variations: Seq[TypeRef] = Seq.empty): ADT = {
    ADT(symbol = baseRef.name, genericArguments = genericArguments, variations = variations, testNamespace)(loc = Seq())
  }

  def caseDefinition(symbol: String, variations: Seq[TypeRef] = Seq()): CaseDefinition = {
    CaseDefinition(symbol, variations, testNamespace)(loc = Seq())
  }

  def genericArgument(name: String): GenericArgument = {
    GenericArgument(name)(loc = Seq())
  }

  def typeRef(name: String, genericArguments: Seq[GenericArgument] = Seq(), typeArguments: Seq[TypeRef] = Seq()): TypeRef = {
    TypeRef(name = name, genericArguments = genericArguments, typeArguments = typeArguments)(loc = Seq())
  }

  def functionDefinition(name: String, arguments: Map[Ref, TypeRef], returns: TypeRef, expression: Expression): Function = {
    Function(testNamespace, name = name, genericArguments = Nil, arguments = arguments, returnType = returns, values = Map.empty, expression = expression)(loc = Seq())
  }

  def functionDefinition(name: String, genericArguments: Seq[GenericArgument], arguments: Map[Ref, TypeRef], returns: TypeRef, expression: Expression): Function = {
    Function(testNamespace, name = name, genericArguments = genericArguments, arguments = arguments, returnType = returns, values = Map.empty, expression = expression)(loc = Seq())
  }

  def functionDefinition(name: String, arguments: Map[Ref, TypeRef], returns: TypeRef, values: Map[Ref, Expression], expression: Expression): Function = {
    Function(testNamespace, name = name, genericArguments = Nil, arguments = arguments, returnType = returns, values = values, expression = expression)(loc = Seq())
  }

}
