package org.halla.testhelpers.parse

import org.halla.compiler.parse.ast.Package
import org.halla.testhelpers.CompilationStepBehaviors.CompilationStepFixtures

object ParserStepFixtures extends CompilationStepFixtures[Package] with ParsedFixtures {

  import org.halla.testhelpers.parse.ParsedResults._

  /**
   * A basic ADT with no type arguments.
   *
   * @return
   */
  override def basicADT: Package = ast.withDefs(
    types = Seq(ast.coinflipTypeDefinition)
  )

  /**
   * A function that adds two integer arguments together.
   *
   * @return
   */
  override def integerAddition: Package = {
    ast.withDefs(
      types = Nil,
      functions = Seq(
        functionDefinition(
          name = "simpleAddition",
          arguments = Map(
            ref("numOne") -> typeRef("Int"),
            ref("numTwo") -> typeRef("Int")
          ),
          returns = typeRef("Int"),
          expression = application(application(operator("+"), ref("numOne")), ref("numTwo"))
        )
      )
    )
  }

  /**
   * A recursive function.
   *
   * @return
   */
  override def recursiveFunction: Package = {
    ast.withDefs(
      types = Nil,
      functions = Seq(
        functionDefinition(
          name = "measureClout",
          arguments = Map(ref("cloutLevel") -> typeRef("Int")),
          returns = typeRef("Int"),
          expression = application(ref("measureClout"), ref("cloutLevel"))
        )
      )
    )
  }

  /**
   * A basic ADT with a type argument.
   *
   * @return
   */
  override def basicADTWithParameters: Package = ast.withDefs(
    types = Seq(ast.maybeITypeDefinition)
  )

  /**
   * A basic ADT used in pattern match statement.
   *
   * @return
   */
  override def basicADTPatternMatch: Package = ast.withDefs(
    types = Seq(ast.coinflipTypeDefinition),
    functions = Seq(ast.coinFlipPatternMatchingImplementation)
  )

  /**
   * Pattern match for a basic ADT with a type argument.
   *
   * @return
   */
  override def patternMatchForBasicADTWithParameters: Package = ast.withDefs(
    types = Seq(ast.maybeITypeDefinition),
    functions = Seq(
      functionDefinition(
        name = "defaultInt",
        arguments = Map(ref("maybeI") -> typeRef("MaybeI")),
        returns = typeRef("Int"),
        expression = {
          matchStatement(
            subject = ref("maybeI"),
            cases = Seq(
              matchCase(
                target = typeRef("Some"),
                arguments = Seq(ref("int")),
                expression = ref("int")
              ),
              matchCase(
                target = typeRef("None"),
                expression = integerLiteral(0)
              )
            )
          )
        }
      )
    )
  )

  /**
   * A function that declares and uses a "val" in it's body.
   *
   * @return
   */
  override def valDefinition: Package = ast.withDefs(
    functions = Seq(
      functionDefinition(
        name = "plusTwo",
        arguments = Map(ref("x") -> typeRef("Int")),
        returns = typeRef("Int"),
        values = Map(ref("plusOne") -> application(
          application(
            operator("+"),
            ref("x")
          ),
          integerLiteral(1)
        )),
        expression = application(
          application(
            operator("+"),
            ref("plusOne")
          ),
          integerLiteral(1)
        )
      )
    )
  )

  /**
   * An ADT with a generic parameter.
   */
  override def genericADT: Package = ast.withDefs(
    types = Seq(ast.optionTypeDefinition)
  )

  /**
   * A function with a generic argument.
   */
  override def genericFunction: Package = ast.withDefs(
    types = Seq(ast.optionTypeDefinition),
    functions = Seq(
      functionDefinition(
        name = "some",
        genericArguments = Seq(genericArgument("A")),
        arguments = Map(ref("a") -> typeRef("A")),
        returns = typeRef("Option", genericArguments = Seq(genericArgument("A"))),
        expression = application(
          ref("Some"),
          ref("a")
        )
      )
    )
  )

  /**
   * A recursive ADT with a generic parameter.
   */
  override def genericRecursiveADT: Package = ???
}
