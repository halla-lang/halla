package org.halla.testhelpers.parse

import cats.data.Chain
import org.halla.compiler.parse.Source
import org.halla.compiler.parse.Source.Programmatic
import org.halla.testhelpers.CompilationStepBehaviors.CompilationStepFixtures

object SourceFixtures extends CompilationStepFixtures[Source] {

  /**
   * A basic ADT with no type arguments.
   *
   * @return
   */
  override def basicADT: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |type CoinFlip = Heads | Tails
      |""".stripMargin
  )

  /**
   * A function that adds two integer arguments together.
   *
   * @return
   */
  override def integerAddition: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |def simpleAddition(numOne: Int, numTwo: Int): Int = {
      |  numOne + numTwo
      |}
      |
      |""".stripMargin
  )


  /**
   * A function that declares and uses a "val" in it's body.
   *
   * @return
   */
  override def valDefinition: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |def plusTwo(x: Int): Int = {
      |  val plusOne = x + 1
      |  plusOne + 1
      |}
      |
      |
      |""".stripMargin
  )

   /**
   * A recursive function.
   *
   * @return
   */
  override def recursiveFunction: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |def measureClout(cloutLevel: Int): Int = {
      |  measureClout cloutLevel
      |}
      |""".stripMargin
  )

  /**
   * A basic ADT used in pattern match statement.
   *
   * @return
   */
  override def basicADTPatternMatch: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |type CoinFlip = Heads | Tails
      |
      |def rateCoinflip(coinflip: CoinFlip): Int = {
      |  coinflip match
      |    Heads ->
      |      1
      |    Tails ->
             0
      |}
      |""".stripMargin
  )

  /**
   * A basic ADT with a type argument.
   *
   * @return
   */
  override def basicADTWithParameters: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |type MaybeI = Some Int | None
      |""".stripMargin
  )

  /**
   * Pattern match for a basic ADT with type parameters.
   *
   * @return
   */
  override def patternMatchForBasicADTWithParameters: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |type MaybeI = Some Int | None
      |
      |def defaultInt(maybeI: MaybeI): Int = {
      |    maybeI match
      |        Some int ->
      |            int
      |        None ->
      |            0
      |}
      |""".stripMargin
  )

  /**
   * An ADT with a generic parameter.
   */
  override def genericADT: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |type Option[A] = Some A
      |               | None
      |
      |""".stripMargin
  )

  /**
   * An ADT with a generic parameter.
   */
  override def genericRecursiveADT: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |type Chain[A] = Single A
      |              | Append A Chain[A]
      |              | Empty
      |
      |""".stripMargin
  )

  /**
   * A function with a generic argument.
   */
  override def genericFunction: Source = Programmatic(
    """package org.halla.MyPackage
      |
      |type Option[A] = Some A
      |               | None
      |
      |def some[A](a: A): Option[A] = {
      |  Some a
      |}
      |
      |""".stripMargin
  )
}
