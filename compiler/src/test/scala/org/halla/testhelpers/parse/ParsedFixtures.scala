package org.halla.testhelpers.parse

import org.halla.compiler.parse.ast.func.Function
import org.halla.compiler.parse.ast.typedef.GenericArgument
import org.halla.compiler.parse.ast.typedef.TypeDefinition.ADT

import scala.collection.SeqOps

trait ParsedFixtures {

  object ast {
    import org.halla.testhelpers.parse.ParsedResults._

    lazy val empty: org.halla.compiler.parse.ast.Package = packag3(namespace = Seq("org", "halla"), name = "MyPackage", imports = Seq(), types = Seq(), functions = Seq())

    def withDefs(types: Seq[ADT] = Seq(), functions: Seq[Function] = Seq()): org.halla.compiler.parse.ast.Package = {
      packag3(namespace = Seq("org", "halla"), name = "MyPackage", imports = Seq(), types = types, functions = functions)
    }

    lazy val noArguments: Function = functionDefinition(
      name = "theAnswer",
      arguments = Map(),
      returns = typeRef("Int"),
      expression = integerLiteral(42)
    )

    // pattern matching

    lazy val coinflipTypeDefinition: ADT = adt(typeRef("CoinFlip"), variations = Seq(typeRef("Heads"), typeRef("Tails")))

    lazy val maybeITypeDefinition: ADT = adt(typeRef("MaybeI"), variations = Seq(
      typeRef("Some", Seq(), Seq(typeRef("Int"))),
      typeRef("None")
    ))

    lazy val optionTypeDefinition: ADT = adt(symbol = "Option", genericArguments = Seq(genericArgument("A")), variations = Seq(
      typeRef("Some", Seq(), Seq(typeRef("A"))),
      typeRef("None")
    ))

    lazy val coinFlipPatternMatchingImplementation: Function = {
      functionDefinition(
        name = "rateCoinflip",
        arguments = Map(ref("coinflip") -> typeRef("CoinFlip")),
        returns = typeRef("Int"),
        expression = {
          matchStatement(
            subject = ref("coinflip"),
            cases = Seq(
              matchCase(
                target = typeRef("Heads"),
                expression = integerLiteral(1)
              ),
              matchCase(
                target = typeRef("Tails"),
                expression = integerLiteral(0)
              )
            )
          )
        }
      )
    }

  }
}
