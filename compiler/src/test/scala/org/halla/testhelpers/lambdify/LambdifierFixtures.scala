package org.halla.testhelpers.lambdify

import cats.data.{Chain, NonEmptyList}
import org.halla.compiler.lambdify.Lambda.Definition.{CaseArgument, FunctionArgument, FunctionReference}
import org.halla.compiler.lambdify.Lambdified.{DefinedFunction, Function}
import org.halla.compiler.lambdify.lambda.Type
import org.halla.compiler.lambdify.lambda.Type.TypeUnion.Anonymous
import org.halla.compiler.lambdify.lambda.Type.{Composite, Native, TypeUnion, Variable}
import org.halla.compiler.lambdify.lambda.composite.Member
import org.halla.compiler.lambdify.lambda.natives.Math.IntegerAddition
import org.halla.compiler.lambdify.lambda.natives.NativeFunction
import org.halla.compiler.lambdify.{Lambda, Lambdified}
import org.halla.compiler.parse.ast.expr.Expression.Match.CaseConstructorMatch
import org.halla.compiler.parse.ast.expr.Expression.Ref
import org.halla.compiler.parse.ast.typedef.TypeDefinition.ADT
import org.halla.compiler.parse.ast.typedef.TypeRef
import org.halla.testhelpers.parse.ParsedFixtures

trait LambdifierFixtures extends ParsedFixtures {

  object lambdified {

    import org.halla.compiler.lambdify.Lambda._


    /**
     * Used to generate test expectations.
     *
     * @param functions that will be added to returned [[Lambdified.Package]]
     * @return
     */
    def apply(functions: Function*): Lambdified.Package = {
      apply(functions, Seq())
    }

    def apply(functions: Iterable[Function] = List(), types: Iterable[Type] = List()): Lambdified.Package = {
      val funcs = Chain.fromSeq(functions.toSeq)
      val typs = types.toList

      Lambdified.Package(funcs, typs, None)
    }

    lazy val empty: Lambdified.Package = apply()

    import org.halla.testhelpers.parse.ParsedResults._

    def application(l: Lambda, r: Lambda): Application = {
      Application(l, r)
    }

    def ref(nat: NativeFunction): Reference = {
      ref(nat.asFunctionReference)
    }

    def ref(reffed: Definition): Reference = {
      Reference(reffed)
    }

    def literal(value: Any, t: Type.Native): Literal = {
      Literal(value, t)(TestParsed)
    }

    def caseArg(symbol: String, ndx: Int, t: Type): CaseArgument = {
      CaseArgument(symbol, ndx, t)(TestParsed)
    }

    def patternMatch(subject: Lambda, cases: PatternMatch.Branch*): PatternMatch = {
      PatternMatch(subject = subject, branches = NonEmptyList.fromListUnsafe(cases))
    }

    def caseConstructorMatch(targetType: Type.Composite, args: List[CaseArgument] = List(), body: Lambda): PatternMatch.Branch = {
      val parsedBranch = CaseConstructorMatch(typeRef(targetType.typeDef.symbol), args.map(a => Ref(a.symbol)(testLines)))(emptyExpression, testNamespace, testLines)

      PatternMatch.CaseConstructorBranch(
        targetType = targetType,
        args = args,
        body = body
      )(parsedBranch)
    }

    lazy val noArguments: Function = {
      DefinedFunction(
        symbol = ast.noArguments.name,
        arguments = List(),
        body = literal(42, Type.Native.I),
        t = Native.I
      )(TestParsed)
    }

    lazy val addFortyTwo: Function = {
      val baseNumber = FunctionArgument("baseNumber", 0, Native.I)(TestParsed)

      DefinedFunction(
        symbol = "addFortyTwo",
        arguments = List(baseNumber),
        body =
          application(
            application(
              ref(IntegerAddition),
              ref(baseNumber)
            ),
            literal(42, Type.Native.I)
          ),
        t = Native.I -> Native.I
      )(TestParsed)
    }

    lazy val addTheAnswer: Function = {

      val baseNumber = FunctionArgument("baseNumber", 0, Native.I)(TestParsed)
      val addFortyTwoRef = FunctionReference(addFortyTwo.symbol, Chain.empty, addFortyTwo.t, Native.I)(TestParsed)

      DefinedFunction(
        symbol = "addTheAnswer",
        arguments = List(baseNumber),
        body = {
          application(
            ref(addFortyTwoRef),
            ref(baseNumber)
          )
        },
        t = Native.I -> Native.I
      )(TestParsed)
    }

    lazy val headsType: Composite = Composite(caseDefinition("Heads"), List.empty)
    lazy val tailsType: Composite = Composite(caseDefinition("Tails"), List.empty)

    lazy val coinFlipType: TypeUnion.Declared = TypeUnion.Declared(ast.coinflipTypeDefinition, NonEmptyList(headsType, List(tailsType)))
    lazy val coinFlipTypes = List(coinFlipType, headsType, tailsType)
    lazy val coinflipFunction: Function = {

      val coinFlipArg = FunctionArgument("coinflip", 0, coinFlipType)(TestParsed)

      DefinedFunction(
        symbol = "rateCoinflip",
        arguments = List(coinFlipArg),
        body = {
          patternMatch(ref(coinFlipArg), cases =
            caseConstructorMatch(headsType, List.empty, body = {
              literal(1, Type.Native.I)
            }),
            caseConstructorMatch(tailsType, List.empty, body = {
              literal(0, Type.Native.I)
            })
          )
        },
        t = coinFlipType -> Native.I
      )(TestParsed)
    }

    lazy val someI: Composite = Composite(caseDefinition("Some", Seq(typeRef("Int"))), List(Member(Native.I)))
    lazy val none: Composite = Composite(caseDefinition("None"), List.empty)
    lazy val maybeIType: TypeUnion.Declared = TypeUnion.Declared(ast.maybeITypeDefinition, NonEmptyList(someI, List(none)))
    lazy val maybeITypes: List[Type] = List(maybeIType, someI, none)

    lazy val some: Composite = Composite(caseDefinition("Some", Seq(typeRef("A"))), List(Member(Variable("A"))))
    lazy val optionType: Type.TypeUnion.Declared = TypeUnion.Declared(ast.optionTypeDefinition, NonEmptyList(
      some,
      List(none)
    ))
    lazy val optionTypes: List[Type] = List(optionType, some, none)

    /**
     * Pattern match for a basic ADT with a type argument.
     *
     * @return
     */
    lazy val defaultIntFunction: DefinedFunction = {
      val maybeI = FunctionArgument("maybeI", 0, maybeIType)(TestParsed)
      val someIArgument = caseArg("int", 0, Native.I)

      DefinedFunction(
        symbol = "defaultInt",
        arguments = List(maybeI),
        body = {
          patternMatch(ref(maybeI),
            caseConstructorMatch(someI, List(someIArgument), body = {
              ref(someIArgument)
            }),
            caseConstructorMatch(none, List.empty, body = {
              literal(0, Type.Native.I)
            })
          )
        },
        t = maybeIType -> Native.I
      )(TestParsed)
    }

  }

}
