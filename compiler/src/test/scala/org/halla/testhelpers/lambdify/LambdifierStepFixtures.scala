package org.halla.testhelpers.lambdify

import cats.data.Chain
import org.halla.compiler.lambdify.Lambda.Definition.{FunctionArgument, FunctionReference, Memo}
import org.halla.compiler.lambdify.Lambdified
import org.halla.compiler.lambdify.Lambdified.DefinedFunction
import org.halla.compiler.lambdify.lambda.Type.{Native, Variable}
import org.halla.compiler.lambdify.lambda.natives.{CaseConstructor, Math}
import org.halla.testhelpers.CompilationStepBehaviors.CompilationStepFixtures
import org.halla.testhelpers.parse.ParsedResults.TestParsed

object LambdifierStepFixtures extends CompilationStepFixtures[Lambdified] with LambdifierFixtures {

  /**
   * A basic ADT with no type arguments.
   *
   * @return
   */
  override def basicADT: Lambdified = lambdified(
    types = lambdified.coinFlipTypes
  )

  /**
   * A function that adds two integer arguments together
   *
   * @return
   */
  override def integerAddition: Lambdified = lambdified(
    functions = Seq({
      import lambdified._

      // the two integer arguments that will be added
      val numOne = FunctionArgument("numOne", 0, Native.I)(TestParsed)
      val numTwo = FunctionArgument("numTwo", 1, Native.I)(TestParsed)

      DefinedFunction(
        symbol = "simpleAddition",
        arguments = List(numOne, numTwo),
        body =
          application(
            application(
              ref(Math.IntegerAddition),
              ref(numOne)
            ),
            ref(numTwo)
          ),
        t = Native.I -> Native.I -> Native.I
      )(TestParsed)
    })
  )

  /**
   * A function that declares and uses a "val" in it's body.
   *
   * @return
   */
  override def valDefinition: Lambdified = lambdified(
    functions = Seq({
      import lambdified._

      // the integer argument for the function
      val x = FunctionArgument("x", 0, Native.I)(TestParsed)

      // this is what the "val" definition represents in the parsed source
      val plusOne = Memo(
        "plusOne",
        application(
          application(
            ref(Math.IntegerAddition),
            ref(x)
          ),
          literal(1, Native.I)
        )
      )(TestParsed)

      DefinedFunction(
        symbol = "plusTwo",
        arguments = List(x),
        body =
          application(
            application(
              ref(Math.IntegerAddition),
              ref(plusOne)
            ),
            literal(1, Native.I)
          ),
        t = Native.I -> Native.I
      )(TestParsed)
    })
  )

  /**
   * A recursive function.
   *
   * @return
   */
  override def recursiveFunction: Lambdified = lambdified(
    functions = Seq({
      import lambdified._

      // the symbol/name of the recursive function
      val recursiveFunctionSymbol = "measureClout"

      // this is the argument that the function accepts
      val cloutLevel = FunctionArgument("cloutLevel", 0, Native.I)(TestParsed)

      // this is the reference for the recursive function call.
      val recursiveRef: FunctionReference = FunctionReference(recursiveFunctionSymbol, Chain(cloutLevel), Native.I -> Native.I, Native.I)(TestParsed)

      DefinedFunction(
        symbol = recursiveFunctionSymbol,
        arguments = List(cloutLevel),
        body = {
          application(
            ref(recursiveRef),
            ref(cloutLevel)
          )
        },
        t = Native.I -> Native.I
      )(TestParsed)
    })
  )

  /**
   * A basic ADT used in pattern match statement.
   *
   * @return
   */
  override def basicADTPatternMatch: Lambdified = lambdified(
    functions = Seq(lambdified.coinflipFunction),
    types = lambdified.coinFlipTypes
  )

  /**
   * A basic ADT with a type argument.
   *
   * @return
   */
  override def basicADTWithParameters: Lambdified = lambdified(
    types = lambdified.maybeITypes
  )

  /**
   * Pattern match for a basic ADT with a parameter.
   *
   * @return
   */
  override def patternMatchForBasicADTWithParameters: Lambdified = lambdified(
    functions = Seq(lambdified.defaultIntFunction),
    types = lambdified.maybeITypes
  )

  /**
   * An ADT with a generic parameter.
   */
  override def genericADT: Lambdified = lambdified(
    functions = Seq(),
    types = lambdified.optionTypes
  )

  /**
   * A function with a generic argument.
   */
  override def genericFunction: Lambdified = lambdified(
    functions = Seq({
      import lambdified._

      // argument to the function that will be applied to the Some constructor
      val arg = FunctionArgument("a", 0, Variable("A"))(TestParsed)

      // the function itself
      DefinedFunction(
        symbol = "some",
        arguments = List(
          arg
        ),
        body =
          application(
            ref(
              // constructs a Some of type Option[A]
              CaseConstructor(some.symbol, some)
            ),
            ref(arg)
          ),
        t = Variable("A") -> optionType
      )(TestParsed)
    }),
    types = lambdified.optionTypes
  )

  /**
   * A recursive ADT with a generic parameter.
   */
  override def genericRecursiveADT: Lambdified = lambdified(
    functions = Seq(),
    types = Seq()
  )
}
