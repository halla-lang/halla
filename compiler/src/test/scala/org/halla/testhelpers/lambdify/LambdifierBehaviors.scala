package org.halla.testhelpers.lambdify

import cats.data.Chain
import org.halla.compiler.common.HResult
import org.halla.compiler.lambdify.Lambdified.{DefinedFunction, Function}
import org.halla.compiler.lambdify.{Lambdified, Lambdifier}
import org.halla.compiler.parse.ast.Package
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{Assertion, Inside}

trait LambdifierBehaviors extends Inside with LambdifierFixtures { this: AnyFlatSpec =>

  def lambdifiableInput(traced: Package, expected: Lambdified): Unit = {

    it should "be successfully lambdified" in {
      Lambdifier.run(traced) match {

        case HResult.Success(actual) =>
          inside (actual) { case Lambdified.Package(actualFunctions, actualTypes, err) =>
            inside(actualFunctions) {
              case Chain(actualFunc) =>
                expected.functions match {
                  case Chain(expectedFunc) =>
                    // if we are comparing one function against one function,
                    // use the following helper to get prettier assertion errors.
                    compareFunctions(actualFunc, expectedFunc)
                  case _ =>
                    // normal assert
                    assert(actualFunctions.toList === expected.functions.toList)
                }
              case _ =>
                // normal assert
                assert(actualFunctions.toList === expected.functions.toList)
            }
            // normal assert
            assert(actualTypes.toList === expected.types.toList)
          }


        case HResult.Failure(errors) =>
          assert(errors.toChain.isEmpty)
      }
    }
  }

  private def compareFunctions(actual: Function, expected: Function): Assertion = {
    inside(actual) { case DefinedFunction(symbol, arguments, body, returnType) =>
      inside(expected) { case expectedFunc: DefinedFunction =>
        assert(symbol === expected.symbol)
        assert(arguments === expectedFunc.arguments)
        assert(body === expectedFunc.body)
        assert(returnType === expected.t)
      }
    }
  }
}
