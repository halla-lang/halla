package org.halla.testhelpers

import org.halla.compiler.common.{CompilationStep, HResult}
import org.halla.testhelpers.CompilationStepBehaviors._
import org.scalatest.flatspec.AnyFlatSpec

trait CompilationStepBehaviors[A, B] {
  this: AnyFlatSpec =>

  implicit def instance: CompilationStep[A, B]

  /**
   * Used to test a series of *valid* expressions to insure they can be successfully compiled by each step in the
   * compilation process.
   *
   * I want to keep this shared set of behaviors easy to add to, so please use other methods for testing invalid input,
   * in order to keep this set of shared specs as simple as possible (I know it's tempting, believe me).
   *
   * @param input    to the compilation step
   * @param expected result of the compilation step upon a successful completion
   */
  def compilationStep(input: CompilationStepFixtures[A], expected: CompilationStepFixtures[B]): Unit = {

    /**
     * helper used to DRY up test cases.
     *
     * @param testCase test case to generate
     */
    def testCompilationStepFor(testCase: TestCase): Unit = {

      val testCaseInput: A = testCase.get(input)
      val expectedResult: B = testCase.get(expected)

      it should s"produce no errors and successfully compile a ${testCase.name}" in {
        instance.run(testCaseInput) match {
          case HResult.Success(actualResult) =>
            assert(actualResult === expectedResult)

          case HResult.Failure(errors) =>
            fail(s"Compilation for a step failed! Reason: \n${errors}")
        }
      }
    }

    // generate the actual test cases
    testCompilationStepFor(BasicADT)
    testCompilationStepFor(IntegerAddition)
    testCompilationStepFor(ValDefinition)
    testCompilationStepFor(RecursiveFunction)
    testCompilationStepFor(BasicADTPatternMatch)
    testCompilationStepFor(BasicADTWithArgs)
    testCompilationStepFor(BasicADTWithArgsPatternMatch)
    testCompilationStepFor(GenericADT)
    testCompilationStepFor(GenericFunction)
  }


}

object CompilationStepBehaviors {

  trait CompilationStepFixtures[A] {

    /**
     * A basic ADT with no type arguments.
     *
     * @return
     */
    def basicADT: A

    /**
     * A function that adds two integer arguments together.
     *
     * @return
     */
    def integerAddition: A

    /**
     * A function that declares and uses a "val" in it's body.
     *
     * @return
     */
    def valDefinition: A

    /**
     * A recursive function.
     *
     * @return
     */
    def recursiveFunction: A

    /**
     * A basic ADT used in pattern match statement.
     *
     * @return
     */
    def basicADTPatternMatch: A

    /**
     * A basic ADT with a parameter.
     *
     * @return
     */
    def basicADTWithParameters: A

    /**
     * Pattern match for a basic ADT with a parameter.
     *
     * @return
     */
    def patternMatchForBasicADTWithParameters: A

    /**
     * An ADT with a generic parameter.
     */
    def genericADT: A

    /**
     * A recursive ADT with a generic parameter.
     */
    def genericRecursiveADT: A

    /**
     * A function with a generic argument.
     */
    def genericFunction: A


  }


  sealed trait TestCase {
    def name: String

    def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A
  }

  case object BasicADT extends TestCase {
    override def name: String = "basic ADT"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.basicADT
  }

  case object IntegerAddition extends TestCase {
    override def name: String = "function that adds two integer arguments together"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.integerAddition
  }

  case object ValDefinition extends TestCase {
    override def name: String = "function that declares and uses a val"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.valDefinition
  }

  case object RecursiveFunction extends TestCase {
    override def name: String = "function that calls itself"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.recursiveFunction
  }

  case object BasicADTPatternMatch extends TestCase {
    override def name: String = "pattern match for a basic ADT"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.basicADTPatternMatch
  }

  case object BasicADTWithArgs extends TestCase {
    override def name: String = "ADT with a parameters"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.basicADTWithParameters
  }

  case object BasicADTWithArgsPatternMatch extends TestCase {
    override def name: String = "pattern match for an ADT with parameters"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.patternMatchForBasicADTWithParameters
  }

  case object GenericADT extends TestCase {
    override def name: String = "ADT with generic parameter"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.genericADT
  }

  case object GenericFunction extends TestCase {
    override def name: String = "function with generic argument"

    override def get[A](compilationStepFixtures: CompilationStepFixtures[A]): A =
      compilationStepFixtures.genericFunction
  }

}
