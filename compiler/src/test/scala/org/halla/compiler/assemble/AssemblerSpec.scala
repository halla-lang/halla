package org.halla.compiler.assemble

import org.halla.compiler.assemble.asm.ASMResult
import org.halla.compiler.assemble.blueprint.PackageBlueprint
import org.halla.testhelpers.assembler.{AssemblyFixtures, DynamicClassLoader}
import org.scalatest.freespec.AnyFreeSpec

class AssemblerSpec extends AnyFreeSpec with AssemblyFixtures {

  def buildAndRunBlueprint(blueprint: PackageBlueprint, entrypoint: String): Any = {
    implicit val loader: DynamicClassLoader = DynamicClassLoader.createLoader()

    // assemble the blueprint
    val assemblyResult: ASMResult = Assembler.compile(blueprint).runtimePackages.head

    // invoke the method
    eval(assemblyResult, entrypoint)
  }

  "Assembler can write bytecode" - {
    "for an empty blueprint" in {
      import org.halla.testhelpers.parse.ParsedResults._

      val emptyBlueprint =
        PackageBlueprint.fromPackage(packag3(namespace = Seq("org", "halla"), name = "MyPackage", imports = Seq()))

      Assembler.compile(emptyBlueprint)

      succeed
    }
  }
}
