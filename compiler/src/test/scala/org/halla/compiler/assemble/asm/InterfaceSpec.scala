package org.halla.compiler.assemble.asm

import org.halla.compiler.assemble.asm.specs.ClassSpecifications
import org.halla.compiler.assemble.asm.specs.ClassSpecifications.{Clazz, Interface}
import org.halla.compiler.assemble.asm.visit.Method
import org.halla.testhelpers.assembler.{AssemblyFixtures, DynamicClassLoader}
import org.scalatest.freespec.AnyFreeSpec

class InterfaceSpec extends AnyFreeSpec with AssemblyFixtures {

  implicit val loader: DynamicClassLoader = DynamicClassLoader.createLoader()

  def verifyMethod(classSpecs: ClassSpecifications, methodName: String)(implicit builderContext: BuilderContext): Any = {
    val clazz = inspectClass(classSpecs)

    // get method to invoke
    val methodToInvoke = clazz.getMethod(methodName)

    // create instance of class to invoke method on.
    val newInstanceOfClazz = clazz.getConstructor().newInstance()

    // result of invocation
    methodToInvoke.invoke(newInstanceOfClazz)
  }

  def inspectClass(classSpecs: ClassSpecifications)(implicit builderContext: BuilderContext): Class[_] = {
    val result = ContextualBuilder.defineClazz(classSpecs)
    inspect(result)
  }

  "Interfaces" - {

    implicit val builderContext: BuilderContext = BuilderContext.fresh()

    val spiderEyes = Method.build("spiderEyes")
      .returns[Integer]
      .abstrct()

    val alienInterfaceSpecs = Interface(nameSpace = Seq("test", "interfacing"), className = "Alien", methods = Seq(spiderEyes))

    val alienInterface = inspectClass(alienInterfaceSpecs)

    "can be generated" in {

      assert(alienInterface.getSimpleName === "Alien")
      assert(alienInterface.getMethods.length === 1)
      assert(alienInterface.getMethods.head.getName === "spiderEyes")
    }

    "can be generated and implemented" in {
      val spiderEyesImpl = Method.build("spiderEyes")
        .returns[Integer]
        .body()
        .push().integer(7).end()
        .ret()

      val alienImplSpecs = Clazz(nameSpace = Seq("test", "interfacing"), className = "OliverTree", implements = Seq(alienInterfaceSpecs), defaultConstructor = true, methods = Seq(spiderEyesImpl))

      val invokeResult = verifyMethod(alienImplSpecs, "spiderEyes")

      assert(invokeResult === 7)
    }
  }
}
