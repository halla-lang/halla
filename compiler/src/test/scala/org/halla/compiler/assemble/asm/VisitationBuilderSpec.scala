package org.halla.compiler.assemble.asm

import org.halla.compiler.assemble.asm.ASMResult.Success
import org.halla.compiler.assemble.asm.specs.ClassSpecifications.Clazz
import org.halla.compiler.assemble.asm.visit.Method
import org.halla.compiler.assemble.asm.visit.invoke.native.Math.Addition
import org.halla.testhelpers.assembler.{AssemblyFixtures, DynamicClassLoader}
import org.objectweb.asm.Type
import org.scalatest.freespec.AnyFreeSpec

class VisitationBuilderSpec extends AnyFreeSpec with AssemblyFixtures {

  lazy val integerType: Type = Type.getType(classOf[java.lang.Integer])
  lazy val stringType: Type = Type.getType(classOf[String])
  lazy val floatType: Type = Type.getType(classOf[java.lang.Float])


  "VisitationBuilder" - {

    val classSpecs = Clazz(nameSpace = Seq("test", "method", "builder"), className = "MyPackage")

    "can create a method" - {

      def verifyMethod(method: Method, expectedResult: Any, args: Object*): Unit = {
        implicit val loader: DynamicClassLoader = DynamicClassLoader.createLoader()

        implicit val builderContext: BuilderContext = BuilderContext.fresh()

        val result = ContextualBuilder.defineClazz(classSpecs.copy(methods = Seq(method)))

        val actualResult = result match {
          case Success(namespace, name, code) =>
            eval(namespace, name, code, method.name, args: _*)
        }

          assert(actualResult === expectedResult)
      }

      "with no arguments" - {
        "that returns void (empty method)" in {
          val method = Method.build("theVoid")
            .static()
            .body()
              .ret()

          verifyMethod(method, null)
        }

        "that returns" - {
          "an integer literal as a single byte" in {
            val method = Method.build("anAnswerWithoutAQuestion")
              .static()
              .returns(integerType)
              .body()
                .push().integer(42).end()
                .ret()

            verifyMethod(method, 42)
          }

          "an integer literal as a short" in {
            val method = Method.build("numberOfTimesIDiedInSekiro")
              .static()
              .returns(integerType)
              .body()
                .push().integer(32767).end()
                .ret()

            verifyMethod(method, 32767)
          }

          "an integer literal" in {
            val method = Method.build("numberOfTimesIActuallyDiedInSekiro")
              .static()
              .returns(integerType)
              .body()
                .push().integer(Integer.MAX_VALUE).end()
                .ret()

            verifyMethod(method, Integer.MAX_VALUE)
          }

          "a string literal" in {
            val method = Method.build("allYouNeedIs")
              .static()
              .returns(stringType)
              .body()
                .push().string("love").end()
                .ret()

            verifyMethod(method, "love")
          }

          "a float literal" in {
            val method = Method.build("floatOn")
              .static()
              .returns(floatType)
              .body()
                .push().float(1.337f).end()
                .ret()

            verifyMethod(method, 1.337f)
          }

          "the result of adding two integers" in {
            val method = Method.build("quickMaths")
              .static()
              .returns[java.lang.Integer]
              .body()
                .invoke(Addition).integer(1).integer(2).end()
                .ret()

            verifyMethod(method, 3)
          }
        }
      }

      "with arguments" - {
        "that returns the original integer argument that was it provided" in {
          val method = Method.build("giveItBack")
            .static()
            .params(Seq(stringType))
            .returns[String]
            .body()
              .push().argument(0).end()
              .ret()

          verifyMethod(method, "yolo", "yolo")
        }
      }
    }
  }
}
