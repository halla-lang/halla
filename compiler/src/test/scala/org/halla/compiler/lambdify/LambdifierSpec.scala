package org.halla.compiler.lambdify

import org.halla.compiler.common.CompilationStep
import org.halla.compiler.parse.ast.Package
import org.halla.testhelpers.CompilationStepBehaviors
import org.halla.testhelpers.lambdify.{LambdifierBehaviors, LambdifierStepFixtures}
import org.halla.testhelpers.parse.{ParsedFixtures, ParserStepFixtures}
import org.scalatest.flatspec.AnyFlatSpec

class LambdifierSpec extends AnyFlatSpec with LambdifierBehaviors with ParsedFixtures with CompilationStepBehaviors[Package, Lambdified] {

  override implicit def instance: CompilationStep[Package, Lambdified] = Lambdifier

  "Lambdifier" should behave like compilationStep(ParserStepFixtures, LambdifierStepFixtures)

  "An empty package" should behave like lambdifiableInput(
    traced = ast.empty,
    expected = lambdified.empty
  )

  "A function with no arguments that returns an integer" should behave like lambdifiableInput(
    traced = ast.withDefs(
      functions = Seq(ast.noArguments)
    ),
    expected = lambdified(
      lambdified.noArguments
    )
  )

}
