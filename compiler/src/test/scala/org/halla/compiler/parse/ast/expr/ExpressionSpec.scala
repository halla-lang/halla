package org.halla.compiler.parse.ast.expr

import org.halla.testhelpers.parse.ParserFixtures
import org.scalatest.freespec.AnyFreeSpec
import parseback.Parser

class ExpressionSpec extends AnyFreeSpec with ParserFixtures[Expression] {

  override val parser: Parser[Expression] = Expression.expression

  import org.halla.testhelpers.parse.ParsedResults._

  "can parse valid expressions" - {

    "as an Access" in {
      parse("""String.append""", access(ref("String"), ref("append")))
    }

    "as a StringLiteral" - {
      "with spaces" in {
        parse("\"nO sOuP fOr U\"", stringLiteral("nO sOuP fOr U"))
      }

      "with escaped quotation marks" in {
        parse(""""an \"effective\" solution"""", stringLiteral("an \"effective\" solution"))
      }
    }

    "as a IntegerLiteral" in {
      parse("1", integerLiteral(1))
      parse("42", integerLiteral(42))
      parse("1337", integerLiteral(1337))
    }

    "as a FloatLiteral" in {
      parse("1.337", floatLiteral(1.337f))
      parse("42.0", floatLiteral(42.0f))
      parse("8008.5", floatLiteral(8008.5f))
    }

    "as a Ref" - {
      "in the form of variable" in {
        parse("someValue", ref("someValue"))
        parse("value", ref("value"))
      }

      "in the form of type variance" in {
        parse("DataAvailable", ref("DataAvailable"))
        parse("Available", ref("Available"))
      }

      "in the form of package" in {
        parse("String", ref("String"))
        parse("Integer", ref("Integer"))
        parse("Codehalla", ref("Codehalla"))
      }
    }

    "as an Application" - {
      "between two references" in {
        parse("increment myNumber", application(left = ref("increment"), right = ref("myNumber")))
      }

      "between a nested Application and a StringValue" in {
        parse("append \"No\" \"Scope\"", application(left = application(left = ref("append"), right = stringLiteral("No")), right = stringLiteral("Scope")))
      }

      "between a nested Application and an Integer" in {
        parse("score \"Ariana Grande\" 4", application(left = application(left = ref("score"), right = stringLiteral("Ariana Grande")), right = integerLiteral(value = 4)))
      }

      "between a nested Application and a Float" in {
        parse("score \"Taylor Swift\" 10.0", application(left = application(left = ref("score"), right = stringLiteral("Taylor Swift")), right = floatLiteral(value = 10.0f)))
      }

      "with a postfix operator" in {
        parse("myNumber + 1", application(application(operator("+"), ref("myNumber")), integerLiteral(1)))
      }
    }

    "as a Match matching" - {
      "a single case" - {
        "with no arguments" in {
          parse("vehicle match\n\tCar -> 1",
            matchStatement(
              subject = ref("vehicle"),
              cases = Seq(
                matchCase(
                  target = typeRef("Car"),
                  expression = integerLiteral(1)
                )
              )
            )
          )
        }

        "with arguments" in {
          parse("vehicle match\n\tCar engine -> rev engine",
            matchStatement(
              subject = ref("vehicle"),
              cases = Seq(
                matchCase(
                  target = typeRef("Car"),
                  arguments = Seq(ref("engine")),
                  expression = application(ref("rev"), ref("engine"))
                )
              )
            )
          )
        }
      }

      "multiple cases" - {
        "with no arguments" in {
          parse("vehicle match\n\tCar -> 42\n\tPlane -> 1337",
            matchStatement(
              subject = ref("vehicle"),
              cases = Seq(
                matchCase(
                  target = typeRef("Car"),
                  expression = integerLiteral(42)
                ),
                matchCase(
                  target = typeRef("Plane"),
                  expression = integerLiteral(1337)
                )
              )
            )
          )
        }

        "with arguments" in {
          parse("vehicle match\n\tCar engine -> rev engine\n\tPlane prop -> spin prop",
            matchStatement(
              subject = ref("vehicle"),
              cases = Seq(
                matchCase(
                  target = typeRef("Car"),
                  arguments = Seq(ref("engine")),
                  expression = application(ref("rev"), ref("engine"))
                ),
                matchCase(
                  target = typeRef("Plane"),
                  arguments = Seq(ref("prop")),
                  expression = application(ref("spin"), ref("prop"))
                )
              )
            )
          )
        }
      }
    }
  }

}