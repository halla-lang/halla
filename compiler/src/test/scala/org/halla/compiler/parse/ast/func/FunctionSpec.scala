package org.halla.compiler.parse.ast.func

import org.halla.testhelpers.parse.ParserFixtures
import org.scalatest.freespec.AnyFreeSpec
import parseback.Parser

class FunctionSpec extends AnyFreeSpec with ParserFixtures[Function] {
  override def parser: Parser[Function] = Function.function

  import org.halla.testhelpers.parse.ParsedResults._


  "can parse a valid function" - {

    "with a single argument" in {
      val function = parse(
        """def addOne(input: Int): Int = {
          |  input + 1
          |}
          |
          |""".stripMargin,
      )

      assert(function.name == "addOne")
    }

    "with a generic argument" in {
      val function = parse(
        """def some[A](a: A): Option[A] = {
          |  Some a
          |}
          |""".stripMargin,
      )

      assert(function.name == "some")
      assert(function.expression == application(
        ref("Some"),
        ref("a")
      ))
    }

    "with a value" in {
      val result = parse(
        """def addTwo(input: Int): Int = {
          |  val plusOne = input + 1
          |  plusOne + 1
          |}
          |""".stripMargin
      )

      assert(result.name == "addTwo")
      assert(result.values == Map(
        ref("plusOne") -> application(
          application(
            operator("+"),
            ref("input")
          ),
          integerLiteral(1)
        )
      ))
    }

    "with values" in {
      val result = parse(
        """def addThree(input: Int): Int = {
          |  val plusOne = input + 1
          |  val plusTwo = plusOne + 1
          |  plusTwo + 1
          |}
          |""".stripMargin
      )

      assert(result.name == "addThree")
      assert(result.values == Map(
        ref("plusOne") -> application(
          application(
            operator("+"),
            ref("input")
          ),
          integerLiteral(1)
        ),
        ref("plusTwo") -> application(
          application(
            operator("+"),
            ref("plusOne")
          ),
          integerLiteral(1)
        )
      ))
    }
  }
}
