package org.halla.compiler.parse.ast.typedef

import org.halla.testhelpers.parse.ParserFixtures
import org.scalatest.freespec.AnyFreeSpec
import parseback.Parser

class TypeDefinitionSpec extends AnyFreeSpec with ParserFixtures[TypeDefinition] {

  override val parser: Parser[TypeDefinition] = TypeDefinition.typeDefinition

  import org.halla.testhelpers.parse.ParsedResults._

  "can parse valid type definition syntax" - {
    "for case types" in {
      parse("type Yolo = Swag | NoScope", adt(typeRef("Yolo"), variations = Seq(typeRef("Swag"), typeRef("NoScope"))))
    }

    "for case types with" - {
      "arguments" in {
        parse(
          "type Vehicle = Car Engine",
          adt(
            baseRef = typeRef("Vehicle"),
            variations = Seq(
              typeRef("Car", typeArguments = Seq(typeRef("Engine")))
            )
          )
        )
      }

      "multiple cases with arguments" in {
        parse(
          "type MaybeI = Some Int | None",
          adt(
            baseRef = typeRef("MaybeI"),
            variations = Seq(
              typeRef("Some", typeArguments = Seq(typeRef("Int"))),
              typeRef("None")
            )
          )
        )
      }

      "generic type arguments" in {
        parse(
          "type Vehicle[P] = Car P",
          genericAdt(
            baseRef = typeRef("Vehicle", typeArguments = Seq(typeRef("P"))),
            genericArguments = Seq(genericArgument("P")),
            variations = Seq(
              typeRef("Car", typeArguments = Seq(typeRef("P")))
            )
          )
        )
      }
    }
  }

}