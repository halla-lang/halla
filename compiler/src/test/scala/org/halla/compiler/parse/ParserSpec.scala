package org.halla.compiler.parse

import org.halla.compiler.common.CompilationStep
import org.halla.compiler.parse.ast.Package
import org.halla.testhelpers.CompilationStepBehaviors
import org.halla.testhelpers.parse.{ParserStepFixtures, SourceFixtures}
import org.scalatest.flatspec.AnyFlatSpec

class ParserSpec extends AnyFlatSpec with CompilationStepBehaviors[Source, Package] {
  override implicit def instance: CompilationStep[Source, Package] = Parser

  "Parser" should behave like compilationStep(SourceFixtures, ParserStepFixtures)

}
