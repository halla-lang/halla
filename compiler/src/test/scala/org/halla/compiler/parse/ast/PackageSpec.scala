package org.halla.compiler.parse.ast

import org.halla.testhelpers.parse.ParserFixtures
import org.scalatest.freespec.AnyFreeSpec
import parseback.Parser

class PackageSpec extends AnyFreeSpec with ParserFixtures[Package] {

  import org.halla.testhelpers.parse.ParsedResults._

  override val parser: Parser[Package] = Package.`package`

  "can parse valid package syntax for a package" - {

    "with no content" in {
      parse("package org.halla.MyPackage", packag3(namespace = Seq("org", "halla"), name = "MyPackage", imports = Seq(), types = Seq(), functions = Seq()))
      parse("package com.yolo.Swag", packag3(namespace = Seq("com", "yolo"), name = "Swag", imports = Seq(), types = Seq(), functions = Seq()))
    }

    "with a single type definition" in {
      val testDefinition = adt(typeRef("Maybe"), Seq(typeRef("Something"), typeRef("Nothing")))

      val code =
        "package org.halla.MyPackage\n" +
          "type Maybe = Something | Nothing"

      parse(
        code
        , packag3(namespace = Seq("org", "halla"), name = "MyPackage", imports = Seq(), types = Seq(testDefinition), functions = Seq())
      )
    }

    "with multiple type definitions" in {
      val chargeDefinition = adt(typeRef("Charge"), variations = Seq(typeRef("Positive"), typeRef("Negative")))
      val coinFlipDefinition = adt(typeRef("CoinFlip"), variations = Seq(typeRef("Heads"), typeRef("Tails")))

      val code =
        "package org.halla.MyPackage\n" +
          "type Charge = Positive | Negative\n" +
          "type CoinFlip = Heads | Tails"

      parse(
        code
        , packag3(namespace = Seq("org", "halla"), name = "MyPackage", imports = Seq(), types = Seq(coinFlipDefinition, chargeDefinition))
      )
    }

    "with a function definition" in {

      val code =
        """package org.halla.MyPackage
          |
          |def increment(num: Int): Int = {
          |  num + 1
          |}
          |
          |""".stripMargin

      parse(
        code
        , packag3(namespace = Seq("org", "halla"), name = "MyPackage", imports = Seq(), functions = Seq(
          functionDefinition(
            name = "increment",
            arguments = Map(ref("num") -> typeRef("Int")),
            returns = typeRef("Int"),
            expression = application(application(operator("+"), ref("num")), integerLiteral(1))
          )
        ))
      )
    }
  }


  "refuses to parse invalid package syntax, such as" - {

    "a space before the word package" in {
      hopelesslyParse(" package org.halla")
    }

    "a newline before the word package" in {
      hopelesslyParse("\npackage org.halla")
    }
  }

}