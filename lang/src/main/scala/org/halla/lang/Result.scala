package org.halla.lang

sealed trait Result[MODEL, MSG <: Any]

object Result {

  /**
    * The pairing of new data with a new set of messages that will be used to update the [[App]].
    *
    * @param model new model
    * @param cmd new updates
    * @tparam MODEL type of model
    * @tparam MSG type of message
    */
  case class CmdResult[MODEL, MSG](model: MODEL, cmd: Cmd[MSG]) extends Result[MODEL, MSG]

  /**
    * An exit is issued in order to terminate the run loop.
    *
    * @param model
    * @tparam MODEL
    */
  case class Exit[MODEL, MSG](model: MODEL) extends Result[MODEL, MSG]

}
