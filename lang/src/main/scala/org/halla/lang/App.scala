package org.halla.lang

import org.halla.lang.Result.{CmdResult, Exit}

abstract class App[MODEL, MSG] {

  def init(): Result[MODEL, MSG]

  def update(msg: MSG, model: MODEL): Result[MODEL, MSG]

  def run(): MODEL = {

    def runLoop(result: Result[MODEL, MSG]): MODEL = {
      result match {

        // normal step
        case CmdResult(model, cmd) =>
          val next = update(cmd.msg, model)

          // return the result of the next iteration
          runLoop(next)

        // final step in app
        case Exit(model) =>
          // print the model for funsies
          println(model.toString)

          // return the resulting model
          model
      }
    }

    // get the initial model
    val initialModel = init()

    // start the run loop
    runLoop(initialModel)
  }
}
