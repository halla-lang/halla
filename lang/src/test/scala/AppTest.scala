import org.halla.lang.Result
import org.halla.lang.Result.Exit
import org.scalatest.freespec.AnyFreeSpec

class AppTest extends AnyFreeSpec {
  "App" - {
    "can be implemented" in {

      class TestApp extends org.halla.lang.App[String, String] {
        override def init(): Result[String, String] = {
          Exit("bye!")
        }

        override def update(msg: String, model: String): Result[String, String] = {
          fail("TestApp shouldn't have called update.")
        }
      }

      val testApp = new TestApp()

      assert(testApp.run() === "bye!")
    }
  }
}
