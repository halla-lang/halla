# Halla #
###### A strongly-typed and purely-functional language optimized for handling mutable state.

[Checkout the website to learn more](https://halla-lang.org/)

![Cats Friendly Badge](https://typelevel.org/cats/img/cats-badge-tiny.png) [![pipeline status](https://gitlab.com/halla-lang/halla/badges/master/pipeline.svg)](https://gitlab.com/halla-lang/halla/commits/master) 

Halla was designed from the ground up to build bullet-proof and debuggable applications with mutable state. Halla helps you control your application's state by giving you a simple-yet-powerful type system and allowing you to write each of your applications as a series of simple, reusable and highly-debuggable *pure* functions.

Halla was inspired by [Haskell](https://www.haskell.org/), [Elm](https://elm-lang.org/) and [Scala](https://www.scala-lang.org/). Hence the name: [**Ha**]skell - E[**l**]m - Sca[**la**] 

## Why?

After spending some time using [Elm](https://elm-lang.org/) to construct front-end UIs for projects, I saw the light. Using Elm's runtime allowed me to build and ship stateful yet bug-free applications much faster than I could in all the previous frameworks I've tried before. On top of that, I found that the code I wrote for those applications was some of the most resuable code I've ever written. What's more, it was pretty clear to me that the outcome of my efforts was not due to any skills (or lack-thereof) I had as a programmer, but was certainly **something** about this strange new language.

Each time I transitioned back into Scala, I felt the freedom and power and it scared me. In hindsight, this was most definitely a good thing, but it still made me miss the carefree attitude I could afford to have while writing code in my heavily restricted and controlled Elm environment. Alas, Elm was built for UIs and writing an API in Elm is just downright unheard of as of the time of me writing this.

So my first thought to solving this problem was to start using Elm with Node.js, but that ended up being more painful than I originally thought. See, Elm's greatest asset is also (in my opinion) it's most critical fault. Elm's philosophy itself. It demands perfection in nearly every aspect. Take the strict requirements imposed on third-party libraries for example. Currently, Elm has great interoperability with Javascript. However, publishing an elm package with Javascript in it is impossible. Now, to be fair, I whole-heartly agree that this is a good idea! It keeps dependencies easily managed and prevents a ton of issues from popping up from under the floorboards.

It does **not** however make life for people who want to write any unorthodox Elm packages easy. Which is kind of the situation I found myself in.      

So, where does that leave us? Well, on it's own, there still isn't a good enough reason to recreate Elm for local applications. However, when we start thinking about running Elm in a compiled language's runtime (rather than Javascript), we now have a good enough reason to rethink a lot of the design decisions that Elm has made. Most important of all these design decisions for us to rethink is of course: Interoperability.

Thus we have the goal of this project, and it's singular reason for existing: Create a language that allows you to write the more complex and stateful pieces of your application's logic in a durable and restrictive language, while using your favorite JVM-based language to handle any unorthodox problems and tap into the JVM's vast sea of time-honored open-source goodness.

## How?

Using the two projects in this repository, along with a ton of help from the community! This repository is the home for the two beating hearts of Halla, they are:
* `compiler` Halla's compiler that turns your Halla code into executables.
* `lang` Halla's core language and runtime. It is required for Halla bytecode execution.

Both projects are currently in their infancy (when I wrote this, `lang` was barely a twinkle in my eye with no tests or usable code), but you can [check out their tests](https://gitlab.com/codehalla/halla/tree/master/compiler/src/test/scala/org/halla/compiler) if you're curious and want to learn more.

## Halla's Compiler

Halla's Compiler operates in 4 distinct phases, which different outputs and inputs. This separation is done for a couple reasons:

Firstly, by decoupling the phases from each other we allow for a "divide and conquer" approach to making updates to Halla's Compiler. If you'd like to contribute to Halla, you don't need to learn the entire codebase to get started. Rather, new contributors can focus on improving just one of these phases, and expand to others as they get more comfortable. 

Secondly, each phase is quite re-usable. Want to work on Syntax highlighting? You'll likely only need to parse an AST from some source, so the **Parse** phase will likely be the only code you will need to familiarize yourself with. Similarly, if all you want to do is work on optimizations to the bytecode Halla produces, you should look no further than the **Assemble** phase.

Here is a list of each of the phases as well as an overview and helpful information.

##### Parse

Entrypoint: [`org.halla.compiler.parse.Parser`](https://gitlab.com/codehalla/halla/blob/master/compiler/src/main/scala/org/halla/compiler/parse/Parser.scala) 

This phase reads Halla source as a String and outputs an AST. This AST can be used for many things (like for relatively easy to compute syntax highlighting for example), but mainly should be used as an input to the Lambdify step.

The output of the Parser is binary; either it succeeds, or fails.

##### Lambdify

Entrypoint: [`org.halla.compiler.lambdify.Lambdifier`](https://gitlab.com/codehalla/halla/blob/master/compiler/src/main/scala/org/halla/compiler/lambdify/Lambdifier.scala) 

The Lambdifier takes in the parsed AST of a package and breaks it down into a simplified Lambda Calculus. It then checks the functions and types to see how they could potentially interact with each other (or themselves in the case of recursion) at runtime. 

By understanding how all the definitions in a package are connected BEFORE types are resolved, type-checking and inference are made much easier for future steps.

The output of the Lambdifier is each of the functions and types it successfully lambdified, as well as any potential errors it found.

##### Assemble

Entrypoint: [`org.halla.compiler.assemble.Assembler`](https://gitlab.com/codehalla/halla/blob/master/compiler/src/main/scala/org/halla/compiler/assemble/Assembler.scala)

The last step in the process, this phase turns a Blueprint produced by the Resolver into bytecode, this bytecode can then be used by the Halla Runtime (defined in the `lang` project) as well any Java/Scala program. 
