# The base stage which builds the projects
#
FROM hseeberger/scala-sbt:8u181_2.12.7_1.2.6 as srcBuilder

# create directory for compiled source
RUN mkdir /halla-src

# copy src from host to new folder on the image
COPY . /halla-src

# set working dir to new src folder
WORKDIR /halla-src

# compile the project
RUN sbt compile

# default behavior is to run tests for the project
ENTRYPOINT ["sbt"]
CMD ["test"]